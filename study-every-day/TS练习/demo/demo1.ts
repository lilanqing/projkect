// let asf: unknown;
// let b: string = asf as string;
// let hd: unknown;
// hd = "houdunren";
// hd = 100;
// //在使用时，TS不知道是什么类型，所以需要使用类型断言进行告之
// let c = (hd as number) + 20;

// 不允许赋值给其他有明确类型的变量。
// 其他类型可以赋值给unknown   unknown类型不能赋值给他有明确类型的变量。

let bar: unknown = 222; // OK
let str = 'aaa';
bar = str;

console.log(bar.msg); // Error
let k_value1: unknown = bar; // OK
let K_value2: any = bar; // OK
let K_value3: number = bar; // Error

str = bar; // Error
