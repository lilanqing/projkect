### 50 个前端开发者在线工具

https://dev.to/haycuoilennao19/50-tool-online-for-front-end-developer-5404

## 生成树状目录结构：tree /f > txt.txt

**npm 查看源地址以及更换源地址**

> npm config get registry

1. (/是根目录 ./是当前目录! ../是上一层目录!)
2. （cd 到下一级目录 cd..返回上一级目录）
3. `npm list -g --depth 0` _(查看全局安装的包)_ `npm root -g` _(查看 npm 全局包安装位置)_

```
如果您使用 ESLint，请安装`eslint-config-prettier` 以使 ESLint 和 Prettier 相互配合。它会关闭所有不必要或可能与 Prettier 冲突的 ESLint 规则。Stylelint 有一个类似的配置：`stylelint-config-prettier`

npm install --save-dev husky lint-staged
npx husky install
npm set-script prepare "husky install"
npx husky add .husky/pre-commit "npx lint-staged"

"lint-staged": {
  "**/*": "prettier --write --ignore-unknown"
}
  注意：如果你使用 ESLint，请确保 lint-staged 在 Prettier 之前运行它(ESLint)，-(eslint在prettier之前运行)-
```

`npx husky add .husky/commit-msg 'npx --no-install commitlint --edit $1`

<!-- npx husky add .husky/commit-msg 'npx --no -- commitlint --edit "$1"' -->

`npx husky add .husky/pre-commit "npm run pre-commit" `
`npx husky add .husky/pre-commit 'npx lint-staged'`

<!-- lint-staged.config.js  -->
<!-- "prepare": "husky install", -->
<!-- "postinstall": "husky install", -->

```
  "lint-staged": {
    "src/**/*.less": [
      "stylelint --fix"
    ],
    "src/**/*.{js,jsx,ts,tsx}": [
      "eslint --ext"
    ],
      "**/*.{js,jsx,tsx,ts,less,md,json}": [
      "prettier --write"
    ]
  },

```

const micromatch = require('micromatch')
module.exports = {
'_': allFiles => {
const match = micromatch(allFiles, ['_.js', '\*.ts'])
return `eslint ${match.join(' ')}`
}
}

## npm 相关

```
name:包名
version:包的版本号，每次发包这个版本号都要改
description：包的描述
private：是否私有，一般都是false
author:作者
license：npm包协议 MIT
keywords：关键字，供npm上模糊搜索到包 []
main: 入口文件

npm login //登录自己的npm账号  llq0802/llq199882..
npm publish
npm unpublish 包名@版本号
```

### git 公钥生成方法：

- ssh-keygen -t rsa -C '你的邮箱' -f ~/.ssh/gitee_id_rsa
- ssh-keygen -t rsa -C '你的邮箱' -f ~/.ssh/github_id_rsa
- ssh-keygen -t rsa -C 'yourEmail@xx.com' -f ~/.ssh/gitlab-rsa

- 在 Git Bash 中输入 ssh-keygen -t rsa -C "你的邮箱" 或者 ssh-keygen -t ed25519 -C "你的邮箱"
- 查看公钥命令：cat ~/.ssh/id_ed25519.pub 或者直接在 C 盘 user 文件中的.ssh 文件中用记事本打开

- 使用命令 touch ~/.ssh/config 在~/.ssh 文件夹下添加 config 文件，可以看到文件夹下面多了一个 config 文件。
- 右键使用记事本打开，复制以下信息添加到 config 文件保存，其中 Host 和 HostName 填写 git 服务器的域名，IdentityFile 填写私钥的路径。

`ssh-keygen -m PEM -t rsa -C 958614130@qq.com -b 4096 -f ~/.ssh/yanfayun_id_rsa`
`ssh-keygen -t rsa -C 958614130@qq.com -f ~/.ssh/yanfayun_id_rsa`

<!-- ssh-keygen -m PEM -t rsa -C 邮箱名 -b 4096 -->

```
# gitee
Host gitee.com
HostName gitee.com
PreferredAuthentications publickey
IdentityFile ~/.ssh/gitee_id_rsa
# github
Host github.com
HostName github.com
PreferredAuthentications publickey
IdentityFile ~/.ssh/github_id_rsa

```

<!-- ssh://git@221.229.103.55:2102/gqtcqsw/youth-cq-front-all.git -->

$ ssh -T git@gitee.com
$ ssh -T git@github.com

- config 内容:

# gitee

Host gitee.com
HostName gitee.com
PreferredAuthentications publickey
IdentityFile ~/.ssh/gitee_id_rsa

# github

Host github.com
HostName github.com
PreferredAuthentications publickey
IdentityFile ~/.ssh/github_id_rsa

# gitlab

Host gitlab.com
HostName gitlab.com
PreferredAuthentications publickey
IdentityFile ~/.ssh/id_rsa

Git 等其配置.gitignore 忽略文件

# 忽略 dbg 文件和 dbg 目录

dbg

# 只忽略 dbg 目录，不忽略 dbg 文件

dbg/

# 只忽略当前目录下的 dbg 文件和目录，子目录的 dbg 不在忽略范围内

/dbg

# 忽略所有.svn 目录

.svn/

# 忽略所有.iml 文件

.iml

# 忽略所有.idea 目录

.idea/

---

- 如果你是使用 https 推拉，配置客户端记住密码：`git config --global credential.helper store`
- git clone 克隆远程仓库地址 自动会将仓库分支关联
- bug 修复分支名-hotfix 功能需求分支名-feature 主分支-master 开发分支-dev 测试分支-test

---

# git 相关

```git
git config --global user.name "李岚清"

```

`git config --global user.email 958614130@qq.com`

---

### git clone -b dev 远程仓库地址

### 本地仓库关联远程仓库: `git remote add origin` **你的远程仓库地址 (如果远程没有分支 就自动新建 master)**

### 取消本地目录下关联的远程库：`git remote remove origin`

### git remote -v 查看远程仓库地址

### git status 然后查看状态

### 当 github 上已经有 master 分支和 dev 分支

- git pull origin dev 本地分支与远程分支 dev 相关联

### 另外一种方法就是先指定本地 master 到远程的 master，然后再去 pull：

git branch --set-upstream-to=origin/远程分支的名字
git pull origin master –allow-unrelated-histories

### 如果远程新建了一个分支，本地没有该分支

`git checkout --track origin/dev` 本地会新建一个分支名叫 dev ，会自动跟踪远程的同名分支 dev。

### 将本地分支和远程分支进行关联

git branch --set-upstream-to=origin/远程分支名 本地分支名

### 自动创建一个新的本地分支，并与指定的远程分支关联起来。

git checkout -b 本地分支名 origin/远程分支名

# 推送单个标签

git push origin <tag_name>

# 推送所有标签

### 当远程没有分支,在本地新建分支并推送到远程

- git checkout -b test
- git push origin test 这样远程仓库中也就创建了一个 test 分支 -第一次 push 远程地址要加-u
- git add .
- git commit -m ‘dev'
- git push -u origin dev

### 然后我们要把 dev 分支的代码合并到 master 分支上 该如何？

本地分支合并方法： -假如我们现在在 dev 合并到 master，

- 首先切换到 master 分支上
  `git checkout master`
  如果是多人开发的话 需要把远程 master 上的代码 pull 下来
  git pull origin master
  如果是自己一个开发就没有必要了，为了保险期间还是 pull
- 然后我们把 dev 分支的代码合并到 master 上
  git merge dev

### 撤销当前的修改：

### 回退到某一版本?

工作区为我们的项目代码

- **git reset 1d7f5d89346 --hard** , **( 工作区 暂存区 本地仓库 都会重置)**
  _切记！！！工作区有未提交的代码时不要用这个命令，因为工作区会回滚，你没有提交的代码就再也找不回了_

- **git reset 1d7f5d89346** , **( 暂存区 本地仓库 都会重置)**

- **git reset 1d7f5d89346 --soft** , **( 本地仓库 会重置)**

- 如何以当前版本为基础，回退指定个**commit：git reset HEAD~X** X 代表你要回退的版本数量，是数字！！！ ( git reset HEAD~1 回退到上一个版本 )

- 如何回退到和远程版本一样：**git reset --hard origin/远程分支名** （强行合并远程指定分支到本地【放弃本地当前未提交的所有修改】）

### 代码没写完怎么切换分支?

`git stash`

- git stash 会把所有未提交的修改（包括暂存的和非暂存的）都保存起来，用于后续恢复当前工作目录。

如果想恢复 `git stash pop`

### 使用 git merge 并自动接受源分支的更改

```git
git merge dev --strategy-option=theirs
```

如果你希望在合并时自动接受源分支的更改，可以使用 -X theirs 选项：

### ：使用 git merge 并自动接受当前分支的更改

如果你希望在合并时自动接受当前分支的更改，可以使用 -X ours 选项：

### 远程仓库切换

ssh 切换 http
git remote set-url origin http://git-scm01.ctyundao.cn/aiRoom/airoom-web.git

http 切换 ssh
git remote set-url origin git@10.238.9.15:aiRoom/airoom-web.git

### 其他 git 命令

`git blame -w --show-name --show-email file.txt` // 显示文件每一行代码的作者和时间
`git log --author="李岚清" --pretty=format:"%h - %an: %s"` // 显示指定作者的所有提交

### 解除复制

```javascript
javascript: 'use strict';
!(function () {
  const t = function (t) {
    t.stopPropagation(), t.stopImmediatePropagation && t.stopImmediatePropagation();
  };
  ['copy', 'cut', 'contextmenu', 'selectstart', 'mousedown', 'mouseup', 'keydown', 'keypress', 'keyup'].forEach(
    (item) => {
      document.documentElement.addEventListener(item, t, { capture: !0 });
    }
  );
  alert('success');
})();
```

### UI 调试

```javascript
(function () {
  const elements = document.body.getElementsByTagName('*');
  const items = [];
  for (let i = 0; i < elements.length; i++) {
    if (elements[i].innerHTML.indexOf('html * { outline: 1px solid red }') != -1) {
      items.push(elements[i]);
    }
  }
  if (items.length > 0) {
    for (let i = 0; i < items.length; i++) {
      items[i].innerHTML = '';
    }
  } else {
    document.body.innerHTML += '<style>html * { outline: 1px solid red }</style>';
  }
})();
```
