//YOUR_QQ_APPID, YOUR_WECHAT_APPID, your AppId 是你在QQ互联 微信开放平台 以及支付宝申请的appid
// 1.QQ登录 cordova plugin add cordova - plugin - qqsdk--variable QQ_APP_ID = YOUR_QQ_APPID
// 2.微信登录 cordova plugin add cordova - plugin - wechat--variable wechatappid = YOUR_WECHAT_APPID
// 3.支付宝支付 cordova plugin add cordova - plugin - alipay - v2--variable APP_ID = [your AppId]
// 4.crosswalk插件 cordova plugin add cordova - plugin - crosswalk - webview
// 5.在android目录下assects目录下添加一个没有后缀名的文件 名字是xwalk - command - line  内容是xwalk--ignore - gpu - blacklist
// 6.在打包的时候 如果两个程序包名一样 不安装cordova - plugin - crosswalk - webview   插件的APK是不能覆盖安装有此插件的APK
// 7.版本插件 cordova plugin add cordova - plugin - app - version
// 8.启动页插件  cordova plugin add cordova - plugin - splashscreen
// 9.获取手机Mac地址 cordova plugin add com - badrit - macaddress
// 10.系统插件  cordova plugin add cordova - plugin - device
// 11.系统键盘插件 cordova plugin add ionic - plugin - keyboard
// 11.字体插件cordova plugin add cordova - plugin - fonts



/**
 * cordova项目文件目录说明
 */

// hooks文件夹没有什么用 不可以改动
// node_nodeule文件夹不可以改动
// platforms文件夹是你的平台代码
// plugins文件夹里面是一些插件
// res是你存放icon和启动页的文件夹
// www里面就是你的代码啦
// .npmignore文件时忽略文件没用
// build.json是我写的json文件下面介绍打包的时候再提
// config.xml这个文件是最重要的 里面是一些配置信息
// package.json和package-lock.json不用动 是自动生成的配置文件
// release-key.keystore文件是打包正式安装包的需要的签名文件

