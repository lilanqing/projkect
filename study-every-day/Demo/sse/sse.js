const http = require("http");


// const str =
//   "SSE 是指 Server-Sent Events（服务器发送事件），是一种用于在客户端和服务器之间单向实时通信的 Web 技术。通过 SSE，服务器可以向客户端推送数据，而无需客户端发起请求。";

const str = `
Fetch API 支持流式响应，这意味着你可以以流的形式读取来自服务器的数据，而不是等待整个响应被接收。这对于处理大型文件或实时数据流非常有用。
要使用 Fetch API 进行流式处理，你可以设置 response 对象的 body 属性，它是一个 ReadableStream 对象。然后，你可以对这个流使用 ReadableStream 的方法，比如 getReader()、read() 和 cancel()。

以下是使用 Fetch API 进行流式处理的基本步骤：

1. 发起请求并获取流
fetch(url, {
  method: 'GET',
  headers: {
    // 可以设置必要的请求头
  },
  // 其他配置...
})
.then(response => {
  if (!response.body) {
    throw new Error('响应中没有流');
  }
  // 获取ReadableStream
  const reader = response.body.getReader();
  // ...
})
.catch(error => {
  console.error('请求失败:', error);
});
2. 读取流中的数据
// 继续上面的代码
const readStream = async (reader) => {
  while (true) {
    const { done, value } = await reader.read(); // 读取流中的数据
    if (done) {
      console.log('流已结束');
      break;
    }
    // 处理接收到的数据块
    console.log(value);
    // 如果是文本数据，可以使用TextDecoder解码
    const decoder = new TextDecoder();
    const decodedData = decoder.decode(value);
    console.log(decodedData);
  }
};

// 开始读取流
readStream(reader);
3. 错误处理和取消流
// 继续上面的代码
reader.closed.catch(error => {
  console.error('读取器关闭失败:', error);
});

// 如果需要取消流
reader.cancel().then(() => {
  console.log('流已取消');
});
示例：流式读取JSON数据
如果你正在从一个以JSON格式流式传输数据的API获取数据，你需要手动处理数据帧的边界。以下是一个示例：

const streamToJson = async (stream) => {
  const reader = stream.getReader();
  let chunks = '';
  
  while (true) {
    const { done, value } = await reader.read();
    if (done) break;
    chunks += new TextDecoder().decode(value);
    // 假设每个JSON对象都以\n结尾
    const jsonChunks = chunks.split('\n');
    for (let i = 0; i < jsonChunks.length - 1; i++) {
      const json = JSON.parse(jsonChunks[i]);
      // 处理每个JSON对象
      console.log(json);
    }
    chunks = jsonChunks[jsonChunks.length - 1];
  }
  
  reader.releaseLock();
};

fetch(url)
.then(response => {
  if (response.ok) {
    return response.body;
  }
  throw new Error('网络响应不正确');
})
.then(stream => streamToJson(stream))
.catch(error => console.error('请求或流处理失败:', error));
流式处理允许你在数据到达时立即处理它，而不是等待整个响应被接收和解析。这对于提高性能和减少内存使用非常有用。
`



const server = http.createServer((req, res) => {
  if (req.url === "/sse") {

    res.writeHead(200, {
      "Content-Type": "text/event-stream",
      "Access-Control-Allow-Origin": "*", // 解决跨域
      "Cache-Control": "no-cache",
      'Connection': 'keep-alive',
    });

    let count = 0;

    const timer = setInterval(() => {
      let info = {};

      const time = new Date().getTime();
      if (count < str.length) {
        info = { type: "keep-alive", count, msg: str[count], time };
      } else {
        info = { type: "close", count, msg: "", time };
        clearInterval(timer);
      }
      count += 1;
      res.write(`data: ${JSON.stringify(info)}\n\n`);
    }, 40);
  }
});

server.listen(3001, "localhost", () => {
  console.log("Server is running at 3001");
});