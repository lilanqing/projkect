import { throttle, transformFromWGSToGCJ, color16, uniqueDeepMap } from './utils.js';

const map = new AMap.Map('map-container', {
  zoom: 16,
  zooms: [4, 20],
  // center: new AMap.LngLat(106.49797771827976, 29.614803915510674), // 麒麟座
  wallColor: '#202124',
  showIndoorMap: false,
  showLabel: true,
  mapStyle: 'amap://styles/light',
  // showBuildingBlock: true,
  // mapStyle: 'amap://styles/macaron',  grey
  // viewMode: '3D',
});

// 同时引入工具条插件，比例尺插件和鹰眼插件
AMap.plugin(['AMap.ToolBar', 'AMap.Scale', 'AMap.HawkEye', 'AMap.MapType', 'AMap.Geolocation'], function () {
  // 在图面添加比例尺控件，展示地图在当前层级和纬度下的比例尺
  map.addControl(new AMap.Scale());

  // // 在图面添加工具条控件，工具条控件集成了缩放、平移、定位等功能按钮在内的组合控件
  // map.addControl(new AMap.ToolBar());

  // // 在图面添加鹰眼控件，在地图右下角显示地图的缩略图
  // map.addControl(new AMap.HawkEye({ isOpen: true }));

  // // 在图面添加类别切换控件，实现默认图层与卫星图、实施交通图层之间切换的控制
  // map.addControl(new AMap.MapType());

  // // 在图面添加定位控件，用来获取和展示用户主机所在的经纬度位置
  // map.addControl(new AMap.Geolocation());
});

let count = 0;
let testList = [
  { lng: '106.53751411388922', name: '夏银强', time: '2023-03-04T00:04:31.360', lat: '29.61995261364163' },
  { lng: '106.54012692204257', name: '夏银强', time: '2023-03-04T00:26:11.840', lat: '29.625905812927645' },
  { lng: '106.5378136974212', name: '夏银强', time: '2023-03-04T00:26:22.080', lat: '29.620134330359' },
  { lng: '106.5371352581017', name: '夏银强', time: '2023-03-04T00:50:15.680', lat: '29.620296489011125' },
  { lng: '106.53525514866823', name: '夏银强', time: '2023-03-04T08:41:18.080', lat: '29.62098487699725' },
  { lng: '106.53678214435311', name: '夏银强', time: '2023-03-04T09:10:39.360', lat: '29.620653325375745' },
  { lng: '106.53431385880465', name: '夏银强', time: '2023-03-04T09:10:39.360', lat: '29.621699215084334' },
  { lng: '106.53613780699153', name: '夏银强', time: '2023-03-04T09:31:49.120', lat: '29.620221508644065' },
  { lng: '106.53667918274436', name: '夏银强', time: '2023-03-04T15:24:55.680', lat: '29.620408592366626' },
  { lng: '106.53659662212526', name: '夏银强', time: '2023-03-04T15:50:52.160', lat: '29.62101890483119' },
  { lng: '106.54397964656417', name: '夏银强', time: '2023-03-04T16:16:58.880', lat: '29.62951400946327' },
  { lng: '106.53828150167344', name: '夏银强', time: '2023-03-04T21:17:11.040', lat: '29.622391478386128' },
  { lng: '106.54286014074805', name: '夏银强', time: '2023-03-04T22:10:15.680', lat: '29.62940808159635' },
];
// testList = uniqueDeepMap(testList);

const testPath = [];
testList
  // .sort((a, b) => a.time - b.time)
  .forEach((item) => {
    const { lng, lat } = item;
    // const { latitude, longitude } = transformFromWGSToGCJ(+lat, +lng);
    // const [longitude, latitude] = coordtransform.wgs84togcj02(lng, lat);
    // testPath.push([longitude, latitude]);
    testPath.push([lng, lat]);
  });

// AMap.convertFrom(testPath, 'gps', function (status, result) {
//   if (result.info === 'ok') {
//     const lnglats = result.locations; // Array.<LngLat>
//     new AMap.Polyline({
//       map: map,
//       path: lnglats,
//       showDir: true,
//       strokeColor: color16(), //线颜色
//       strokeOpacity: 1, //线透明度
//       strokeWeight: 8, //线宽
//       lineCap: 'round',
//     });
//     new AMap.Marker({
//       map: map,
//       position: lnglats[0],
//       // angle: 45,
//       icon: 'https://a.amap.com/jsapi_demos/static/demo-center-v2/car.png',
//       // offset: new AMap.Pixel(-13, -26),
//       anchor: 'top-center',
//     });
//     for (let index = 0; index < lnglats.length; index++) {
//       const point = lnglats[index];
//       new AMap.Marker({
//         map: map,
//         position: point,
//         anchor: 'bottom-center',
//         label: {
//           direction: 'top',
//           content: `<div class='info'>
//                       <div>序号：${index + 1}</div>
//                       <div>时间：2023-3-4</div>
//                     </div>`, //设置文本标注内容
//         },
//       });
//     }

//     const text = new AMap.Text({
//       map: map,
//       position: testPath[Math.floor(testPath.length / 2)],
//       text: testList[0].name,
//       anchor: 'bottom-center',
//       style: { 'background-color': 'red' },
//     });

//     map.setFitView();
//   }
// });

// new AMap.Polyline({
//   map: map,
//   path: testPath,
//   showDir: true,
//   // strokeColor: '#28F', //线颜色
//   strokeColor: color16(), //线颜色
//   strokeOpacity: 1, //线透明度
//   strokeWeight: 8, //线宽
//   lineCap: 'round',
// });
new AMap.Marker({
  map: map,
  position: testPath[0],
  // angle: 45,
  icon: 'https://a.amap.com/jsapi_demos/static/demo-center-v2/car.png',
  // offset: new AMap.Pixel(-13, -26),
  anchor: 'top-center',
});

for (let i = 0; i < testList.length; i++) {
  const item = testList[i];
  // <div>时间：${new Date().toLocaleString()}</div>
  const curTime = new Date(item.time).toLocaleString();

  if (curTime == 'Invalid Date') {
    count++;
  }

  new AMap.Marker({
    map: map,
    position: testPath[i],
    anchor: 'bottom-center',
    label: {
      direction: 'top',
      content: `<div class='amap-label-content'>
                    <div>时间：${curTime}</div>
                </div>`, //设置文本标注内容
    },
    // <div>序号：${i + 1}</div>
  });
}

new AMap.Text({
  map: map,
  position: testPath[Math.floor(testList.length / 2)],
  text: `${testList[0].name} - ${testList.length}个点位`,
  anchor: 'bottom-center',
  style: { 'background-color': 'red' },
});
map.setFitView();
console.log('count', count);

// const lineArr = [
//   [116.478935, 39.997761],
//   [116.478939, 39.997825],
//   [116.478912, 39.998549],
//   [116.478912, 39.998549],
//   [116.478998, 39.998555],
//   [116.478998, 39.998555],
//   [116.479282, 39.99856],
//   [116.479658, 39.998528],
//   [116.480151, 39.998453],
//   [116.480784, 39.998302],
//   [116.480784, 39.998302],
//   [116.481149, 39.998184],
//   [116.481573, 39.997997],
//   [116.481863, 39.997846],
//   [116.482072, 39.997718],
//   [116.482362, 39.997718],
//   [116.483633, 39.998935],
//   [116.48367, 39.998968],
//   [116.484648, 39.999861],
// ];
// // 经纬度坐标转成容器像素坐标
// function lnglat2container(lng, lat) {
//   let lnglat = new AMap.LngLat(lng, lat);
//   let pixel = map.lngLatToContainer(lnglat);
//   return pixel.toString();
// }

// // 容器像素坐标转成经纬度坐标
// function container2lnglat(x, y) {
//   let pixel = new AMap.Pixel(x, y);
//   let lnglat = map.containerToLngLat(pixel);
//   return lnglat;
// }
// const NUMBER_100 = 100;
// let rectOverlayGroup = []; //屏幕可视区的栅格组
// let activeRect = []; // 选中的栅格

// /**
//  * 实现栅格
//  * @param {*} map
//  */
// function getViewPath(map) {
//   // new AMap.LngLat(106.49797771827976, 29.614803915510674),
//   // new AMap.LngLat(106.49903337509201, 29.61392061112083);
//   const boundsView = map.getBounds();
//   let paths = []; //所以栅格的bounds
//   let gridList = []; // 所有栅格实例

//   const leftDownLnglat = new AMap.LngLat(boundsView.southWest.lng, boundsView.southWest.lat); //左下
//   const rightUpLnglat = new AMap.LngLat(boundsView.northEast.lng, boundsView.northEast.lat); //右上
//   const leftUpLnglat = new AMap.LngLat(boundsView.southWest.lng, boundsView.northEast.lat); //左上
//   const rightDownLnglat = new AMap.LngLat(boundsView.northEast.lng, boundsView.southWest.lat); //右下
//   const initP1 = new AMap.LngLat(106.497977, 29.614803);
//   const initP2 = initP1.offset(100, 0);
//   const initP3 = initP2.offset(0, 100);
//   const initP4 = initP1.offset(100, 0);

//   const isLeftDownPointInRing = boundsView.contains(initP1);
//   const isRightDownPointInRing = boundsView.contains(initP2);
//   const isRightUptPointInRing = boundsView.contains(initP3);
//   const isLeftUptPointInRing = boundsView.contains(initP4);

//   // if (!isLeftDownPointInRing && !isRightDownPointInRing && !isRightUptPointInRing && !isLeftUptPointInRing) {
//   //   if (initP1.lng > rightUpLnglat.lng && initP1.lat > rightDownLnglat.lat && initP1.lat < rightUpLnglat.lat) {
//   //     console.log('在屏幕外右并且在屏幕垂直范围内');
//   //     let pahtsLeftUp = getGird('leftUp', initP1, map);
//   //     let pathsLeftDown = getGird('leftDown', initP1, map);
//   //     paths = [...pahtsLeftUp, ...pathsLeftDown];
//   //   }

//   //   if (initP1.lng > rightUpLnglat.lng && initP1.lat > rightUpLnglat.lat) {
//   //     console.log('在屏幕外右上');
//   //     paths = getGird('leftDown', initP3, map);
//   //   }

//   //   if (initP1.lng > rightUpLnglat.lng && initP1.lat < rightDownLnglat.lat) {
//   //     console.log('在屏幕外右下');
//   //     paths = getGird('leftUp', initP2, map);
//   //   }

//   //   if (initP3.lng < leftDownLnglat.lng && initP3.lat < rightUpLnglat.lat && initP3.lat > leftDownLnglat.lat) {
//   //     console.log('在屏幕外左并且在屏幕垂直范围内');
//   //     let pathsRightDown = getGird('rightDown', initP3, map);
//   //     let pathsRightUp = getGird('rightUp', initP3, map);
//   //     paths = [...pathsRightDown, ...pathsRightUp];
//   //   }

//   //   if (initP3.lng < leftDownLnglat.lng && initP3.lat > rightUpLnglat.lat) {
//   //     console.log('在屏幕外左上');
//   //     paths = getGird('rightDown', initP4, map);
//   //   }

//   //   if (initP3.lng < leftDownLnglat.lng && initP3.lat < leftDownLnglat.lat) {
//   //     console.log('在屏幕外左下');
//   //     paths = getGird('rightUp', initP1, map);
//   //   }

//   //   if (initP1.lat > rightUpLnglat.lat && initP3.lng > leftDownLnglat.lng && initP1.lng < rightUpLnglat.lng) {
//   //     console.log('在屏幕外上面并且在屏幕水平范围内');
//   //     let pathsLeftDown = getGird('leftDown', initP1, map);
//   //     let pathsRightDown = getGird('rightDown', initP1, map);
//   //     paths = [...pathsLeftDown, ...pathsRightDown];
//   //   }

//   //   if (initP3.lat < leftDownLnglat.lat && initP3.lng > leftDownLnglat.lng && initP1.lng < rightUpLnglat.lng) {
//   //     console.log('在屏幕外下面并且在屏幕水平范围内');
//   //     let pathsLeftUp = getGird('leftUp', initP1, map);
//   //     let pathsRightUp = getGird('rightUp', initP1, map);
//   //     paths = [...pathsLeftUp, ...pathsRightUp];
//   //   }

//   //   // paths = getGird('rightUp', leftDownLnglat, map);
//   // } else {
//   //   console.log('在屏幕内');
//   //   let pathsRightUp = getGird('rightUp', initP1, map);
//   //   let pahtsLeftUp = getGird('leftUp', initP1, map);
//   //   let pathsLeftDown = getGird('leftDown', initP1, map);
//   //   let pathsRightDown = getGird('rightDown', initP1, map);
//   //   paths = [...pathsRightUp, ...pahtsLeftUp, ...pathsLeftDown, ...pathsRightDown];
//   //   // paths = pathsRightDown;
//   // }

//   paths = getGridPaths(initP1, map);

//   for (let index = 0; index < paths.length; index++) {
//     let rectangle = new AMap.Rectangle({
//       bounds: paths[index],
//       strokeColor: '#353535',
//       strokeWeight: 1,
//       strokeOpacity: 1,
//       strokeStyle: 'solid',
//       fillColor: 'blue',
//       fillOpacity: 0,
//       cursor: 'pointer',
//       zIndex: 50,
//       extData: {
//         id: Math.random().toString(36).slice(2),
//         gridId: paths[index].southWest.toString() + '-' + paths[index].northEast.toString(),
//       },
//     });
//     gridList.push(rectangle);
//     rectangle.on('click', function (e) {
//       console.log('e-rectangle', e.lnglat.toString(), this.getExtData());
//       // alert(`当前栅格坐标：${e.lnglat.toString()}`);
//       alert(`当前栅格坐标：${this.getExtData().gridId}\n当前地图层级：${map.getZoom()}`);
//     });
//   }

//   rectOverlayGroup = new AMap.OverlayGroup(gridList);

//   map.add(rectOverlayGroup);
// }

// // ================初始化====================
// if (map.getZoom() >= 16.5) {
//   getViewPath(map);
// }

// let activePaths = [
//   [
//     [106.50107398446778, 29.614768891789858],
//     [106.50212966677509, 29.613885612978446],
//   ],

//   [
//     [106.50418363768387, 29.61563563602729],
//     [106.50523935536, 29.614752383355057],
//   ],
//   [
//     [106.49694562833633, 29.614815574036705],
//     [106.49800127664933, 29.613932261121274],
//   ],
// ];

// for (let j = 0; j < activePaths.length; j++) {
//   const item = activePaths[j];
//   const activeRectangle = new AMap.Rectangle({
//     bounds: new AMap.Bounds(item[0], item[1]),
//     strokeColor: '#1890ff',
//     strokeWeight: 1,
//     strokeOpacity: 1,
//     strokeStyle: 'solid',
//     fillColor: 'red',
//     fillOpacity: 0.6,
//     cursor: 'pointer',
//     zIndex: 2,
//     extData: {
//       id: Math.random().toString(36).slice(2),
//     },
//   });
//   activeRect.push(activeRectangle);
// }
// let initRectangle = new AMap.Rectangle({
//   bounds: new AMap.Bounds(
//     new AMap.LngLat(106.49797771827976, 29.614803915510674),
//     new AMap.LngLat(106.49797771827976, 29.614803915510674).offset(0, 100).offset(100, 0)
//   ),
//   strokeColor: '#1890ff',
//   strokeWeight: 1,
//   strokeOpacity: 1,
//   strokeStyle: 'solid',
//   fillColor: 'blue',
//   fillOpacity: 0.5,
//   cursor: 'pointer',
//   zIndex: 2,
//   extData: {
//     id: Math.random().toString(36).slice(2),
//   },
// });
// map.add(initRectangle);
// map.add(activeRect);

// // dragend
// // moveend
// map.on(
//   'moveend',
//   throttle(function (e) {
//     // map.remove(activeRect);
//     map.remove(rectOverlayGroup);
//     if (map.getZoom() >= 16.5) {
//       // Promise.resolve().then(() => {
//       getViewPath(map);

//       // });
//     }
//   }, 300)
// );
// map.on(
//   'resize',
//   throttle(function (e) {
//     // map.remove(activeRect);
//     map.remove(rectOverlayGroup);
//     if (map.getZoom() >= 16.5) {
//       // Promise.resolve().then(() => {
//       getViewPath(map);

//       // });
//     }
//   }, 300)
// );

// let mousetool;
// map.plugin(['AMap.MouseTool'], function () {
//   mousetool = new AMap.MouseTool(map);
//   // 使用鼠标工具，在地图上画标记点
// });
// // mousetool.rectangle();

// mousetool.on('draw', function (e) {
//   console.log('e ', e);
//   e.obj.hide();
//   e.obj.destroy();
//   // mousetool.close(true);
// });

// var heatmap;
// var points = [
//   { lng: 116.191031, lat: 39.988585, count: 10 },
//   { lng: 116.389275, lat: 39.925818, count: 11 },
//   { lng: 116.287444, lat: 39.810742, count: 70 },
//   { lng: 116.481707, lat: 39.940089, count: 90 },
//   { lng: 116.410588, lat: 39.880172, count: 80 },
//   { lng: 116.394816, lat: 39.91181, count: 100 },
//   { lng: 116.416002, lat: 39.952917, count: 50 },
// ];
// map.plugin(['AMap.HeatMap'], function () {
//   //加载热力图插件
//   heatmap = new AMap.HeatMap(map, {
//     radius: 25, //给定半径
//     opacity: [0, 0.8],
//     gradient: {
//       0.5: 'blue',
//       0.65: 'rgb(117,211,248)',
//       0.7: 'rgb(0, 255, 0)',
//       0.9: '#ffea00',
//       1.0: 'red',
//     },
//   }); //在地图对象叠加热力图
//   heatmap.setDataSet({ data: points, max: 100 }); //设置热力图数据集
//   //具体参数见接口文档
// });

// /**
//  * 获取可视区范围内的所有栅格
//  * @param {*} initPoint 初始点
//  * @param {*} map 地图实例
//  * @param {*} size 栅格大小
//  * @return {*} 栅格经度纬度数组
//  */
// function getGridPaths(initPoint, map, size = 100) {
//   const paths = [];

//   const boundsView = map.getBounds();
//   const leftDownLnglat = new AMap.LngLat(boundsView.southWest.lng, boundsView.southWest.lat); //左下
//   const rightUpLnglat = new AMap.LngLat(boundsView.northEast.lng, boundsView.northEast.lat); //右上
//   const leftUpLnglat = new AMap.LngLat(boundsView.southWest.lng, boundsView.northEast.lat); //左上
//   const rightDownLnglat = new AMap.LngLat(boundsView.northEast.lng, boundsView.southWest.lat); //右下

//   let leftIniPoint = new AMap.LngLat(leftDownLnglat.lng, initPoint.lat);
//   let downInitPoint = new AMap.LngLat(initPoint.lng, leftDownLnglat.lat);

//   const isRight = initPoint.lng - leftDownLnglat.lng > 0;
//   const isUp = initPoint.lat - leftDownLnglat.lat > 0;

//   const xDistanceCount = isRight
//     ? Math.ceil(leftIniPoint.distance(initPoint) / size)
//     : Math.floor(leftIniPoint.distance(initPoint) / size);
//   const yDistanceCount = isUp
//     ? Math.ceil(downInitPoint.distance(initPoint) / size)
//     : Math.floor(downInitPoint.distance(initPoint) / size);

//   const xDistance = !isRight ? xDistanceCount * size : -(xDistanceCount * size);
//   const yDistance = !isUp ? yDistanceCount * size : -(yDistanceCount * size);

//   const newLeftIniPoint = initPoint.offset(xDistance, 0);
//   const newDownInitPoint = initPoint.offset(0, yDistance);
//   const newLeftDownLng = new AMap.LngLat(newLeftIniPoint.lng, newDownInitPoint.lat);

//   let currentRenderLnglat = newLeftDownLng;

//   let i = 0;
//   while (true) {
//     if (currentRenderLnglat.lat > rightUpLnglat.lat) {
//       break;
//     }
//     i++;
//     while (true) {
//       let nextRightLnglat = currentRenderLnglat.offset(size, 0);
//       if (nextRightLnglat.lng <= rightUpLnglat.lng) {
//         let p1 = currentRenderLnglat;
//         let p2 = p1.offset(size, 0);
//         let p3 = p2.offset(0, size);
//         paths.push(new AMap.Bounds(p3, p1));
//         currentRenderLnglat = nextRightLnglat;
//       } else {
//         currentRenderLnglat = newLeftDownLng.offset(0, size * i);
//         if (paths.length > 0) {
//           paths.push(
//             new AMap.Bounds(
//               new AMap.LngLat(paths.at(-1).southWest.lng, paths.at(-1).southWest.lat).offset(size, 0),
//               new AMap.LngLat(paths.at(-1).northEast.lng, paths.at(-1).northEast.lat).offset(size, 0)
//             )
//           );
//         } else {
//           // 处理边界
//           const p1 = initPoint;
//           const p2 = p1.offset(size, 0);
//           const p3 = p2.offset(0, size);
//           paths.push(new AMap.Bounds(p1, p3));
//         }

//         break;
//       }
//     }
//   }

//   return paths;
// }

// /**
//  * @param {*} direction 渲染方向
//  * @param {*} initPoint 初始渲染点
//  * @param {*} map 地图
//  * @return {*}
//  */
// function getGird(direction, initPoint, map) {
//   const boundsView = map.getBounds();
//   const leftDownLnglat = new AMap.LngLat(boundsView.southWest.lng, boundsView.southWest.lat); //左下
//   const rightUpLnglat = new AMap.LngLat(boundsView.northEast.lng, boundsView.northEast.lat); //右上
//   const leftUpLnglat = new AMap.LngLat(boundsView.southWest.lng, boundsView.northEast.lat); //左上
//   const rightDownLnglat = new AMap.LngLat(boundsView.northEast.lng, boundsView.southWest.lat); //右下
//   const paths = [];
//   let currentXLnglat = initPoint,
//     i = 0,
//     offsetX,
//     offsetY;
//   if (direction === 'rightUp') {
//     offsetX = NUMBER_100;
//     offsetY = NUMBER_100;
//   }

//   if (direction === 'leftUp') {
//     offsetX = -NUMBER_100;
//     offsetY = NUMBER_100;
//   }

//   if (direction === 'rightDown') {
//     offsetX = NUMBER_100;
//     offsetY = -NUMBER_100;
//   }

//   if (direction === 'leftDown') {
//     offsetX = -NUMBER_100;
//     offsetY = -NUMBER_100;
//   }

//   while (true) {
//     if (direction === 'rightUp' && currentXLnglat.lat > rightUpLnglat.lat) {
//       break;
//     }

//     if (direction === 'leftUp' && currentXLnglat.lat > leftUpLnglat.lat) {
//       break;
//     }

//     if (direction === 'rightDown' && currentXLnglat.lat < rightDownLnglat.lat) {
//       break;
//     }

//     if (direction === 'leftDown' && currentXLnglat.lat < leftDownLnglat.lat) {
//       break;
//     }

//     i++;

//     while (true) {
//       let nextLnglat = currentXLnglat.offset(offsetX, 0);
//       if (
//         (direction === 'rightUp' && nextLnglat.lng < rightUpLnglat.lng) ||
//         (direction === 'leftUp' && nextLnglat.lng > leftUpLnglat.lng) ||
//         (direction === 'rightDown' && nextLnglat.lng < rightUpLnglat.lng) ||
//         (direction === 'leftDown' && nextLnglat.lng > leftUpLnglat.lng)
//       ) {
//         const p1 = currentXLnglat;
//         const p2 = p1.offset(offsetX, 0);
//         const p3 = p2.offset(0, offsetY);
//         paths.push(new AMap.Bounds(p1, p3));
//         currentXLnglat = nextLnglat;
//       } else {
//         currentXLnglat = initPoint.offset(0, offsetY * i); // 横线铺满 则向上或向下重复铺
//         // console.log('paths', paths.length);
//         if (paths.length > 0) {
//           paths.push(
//             new AMap.Bounds(
//               new AMap.LngLat(paths.at(-1).southWest.lng, paths.at(-1).southWest.lat).offset(offsetX, 0),
//               new AMap.LngLat(paths.at(-1).northEast.lng, paths.at(-1).northEast.lat).offset(offsetX, 0)
//             )
//           );
//         } else {
//           // 处理边界
//           const p1 = initPoint;
//           const p2 = p1.offset(offsetX, 0);
//           const p3 = p2.offset(0, offsetY);
//           paths.push(new AMap.Bounds(p1, p3));
//         }

//         break;
//       }
//     }
//   }

//   return paths;
// }
