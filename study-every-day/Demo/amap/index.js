import { transformFromGCJToWGS } from './utils.js';

const map = new AMap.Map('map-container', {
  zoom: 17,
  zooms: [2, 20],
  center: new AMap.LngLat(106.49797771827976, 29.614803915510674),
  // pitch: 40,
  wallColor: '#202124',
  // roofColor: '#273136',
  showBuildingBlock: true,
  showIndoorMap: false,
  showLabel: true,
  // mapStyle: 'amap://styles/macaron',  grey
  mapStyle: 'amap://styles/light',
  viewMode: '3D',
  // layers: [
  //   // AMap.createDefaultLayer(),
  //   // new AMap.Buildings({
  //   //   zooms: [16, 18],
  //   //   zIndex: 10,
  //   //   heightFactor: 2, //2倍于默认高度，3D下有效
  //   // }),
  // ],
});

// map.plugin(['AMap.MouseTool'], function () {
//   let mousetool = new AMap.MouseTool(map);
//   // 使用鼠标工具，在地图上画标记点
//   mousetool.rectangle();
// });
// new AMap.PolygonEditor(map);

// const flexibleLayer = new AMap.TileLayer.Flexible({
//   cacheSize: 300,
//   zIndex: 200,
//   tileSize: 128,
//   // zooms: [1, 20],
//   createTile: function (x, y, z, success, fail) {
//     const c = document.createElement('canvas');
//     c.width = c.height = 128 * window.devicePixelRatio;
//     const cxt = c.getContext('2d');
//     cxt.font = '12px Verdana';
//     cxt.fillStyle = '#1395ff';
//     cxt.strokeStyle = '#1395ff';
//     cxt.strokeRect(0, 0, c.width, c.width);
//     cxt.fillText([x, y, z].join(','), 10, 30);

//     // 通知API切片创建完成
//     success(c);
//   },
// });
// flexibleLayer.setMap(map);

// const res = AMap.GeometryUtil.distance(
//   new AMap.LngLat(116.12221, 39.10231),
//   new AMap.LngLat(116.12321, 39.10231)
// );

// console.log('res ', res);

// 经纬度坐标转成容器像素坐标
function lnglat2container(lng, lat) {
  let lnglat = new AMap.LngLat(lng, lat);
  let pixel = map.lngLatToContainer(lnglat);
  return pixel.toString();
}

// 容器像素坐标转成经纬度坐标
function container2lnglat(x, y) {
  let pixel = new AMap.Pixel(x, y);
  let lnglat = map.containerToLngLat(pixel);
  return lnglat;
}

// let boundsView = map.getBounds();
// console.log(lnglat2container(boundsView.southWest.lng, boundsView.northEast.lat));
// console.log('boundsView', boundsView);
// console.log('左上', [boundsView.southWest.lng, boundsView.northEast.lat]);

// console.log('右上', [boundsView.northEast.lng, boundsView.northEast.lat]);

// console.log('左下', [boundsView.southWest.lng, boundsView.southWest.lat]);

// console.log('右下', [boundsView.northEast.lng, boundsView.southWest.lat]);
// console.log('boundsView.northEast右上', boundsView.northEast.toString());
// console.log('boundsView.southWest左下', boundsView.southWest.toString());

// let p0 = new AMap.LngLat(106.025187, 28.963041);
// let p1 = p0.offset(100, 0);
// const dis = Math.round(p0.distance(p1));
// 返回线段 p0-p1-p2 的实际长度，单位：米
// let dis = AMap.GeometryUtil.distanceOfLine([p0, p1]);
// console.log(' p0', p0.getLng(), p0.getLat());
// console.log(' p1', p1.getLng(), p1.getLat());
// console.log(' p0 p1相距', `${dis}米`);

function getViewPath(map) {
  const boundsView = map.getBounds();
  const leftDownLnglat = new AMap.LngLat(boundsView.southWest.lng, boundsView.southWest.lat); //左下
  const rightUpLnglat = new AMap.LngLat(boundsView.northEast.lng, boundsView.northEast.lat); //右上
  const leftUpLnglat = new AMap.LngLat(boundsView.southWest.lng, boundsView.northEast.lat); //左上
  const RightDownLnglat = new AMap.LngLat(boundsView.northEast.lng, boundsView.southWest.lat); //右下
  // console.log('右上坐标 ', lnglat2container(rightUpLnglat.lng, rightUpLnglat.lat));
  // console.log('左下坐标 ', lnglat2container(leftDownLnglat.lng, leftDownLnglat.lat));
  // console.log('左上坐标 ', lnglat2container(leftUpLnglat.lng, leftUpLnglat.lat));
  // console.log('右下坐标 ', lnglat2container(RightDownLnglat.lng, RightDownLnglat.lat));
  // console.log(' ce', document.getElementById('map-container').getBoundingClientRect());
  // console.log('leftUpLnglat', new AMap.LngLat(leftUpLnglat[0], leftUpLnglat[1]));
  // console.log('rightUpLnglat', new AMap.LngLat(rightUpLnglat[0], rightUpLnglat[1]));
  // const p1 = new AMap.LngLat(leftUpLnglat[0], leftUpLnglat[1]);
  // const p2 = new AMap.LngLat(rightUpLnglat[0], rightUpLnglat[1]);
  // const dis = leftDownLnglat.distance(RightDownLnglat);
  // console.log('dis ', Math.ceil(dis / 100));
  // console.log(' p1 p2相距', `${dis}米`);

  // console.log((rightUpLnglat[0] - leftUpLnglat[0]) / (Math.ceil(dis / 100)));

  // 0.00103  纬度不变,经度增加0.00103 距离增加100米
  // 0.0009    经度不变,纬度度增加 0.0009 距离增加100米

  // 29.499469
  // 106.422565
  // 106.587359
  // 29.567576
  // const dis = leftDownLnglat.distance(leftUpLnglat);
  // console.log(' p1 p2相距', `${dis}米`);
  // console.log('currentLnglat.lng ', currentLnglat.lng);
  // console.log('currentLnglat.offset(100, 0).lng ', currentLnglat.offset(100, 0).lng);

  let path = [];
  // let currentXLnglat = leftDownLnglat;
  // for (let i = 0; i < 200; i++) {
  //   if (currentXLnglat.lat > rightUpLnglat.lat) {
  //     break;
  //   }
  //   for (let k = 0; k < 220; k++) {
  //     let nextRightLnglat = currentXLnglat.offset(100, 0);
  //     if (nextRightLnglat.lng <= rightUpLnglat.lng) {
  //       let p1 = currentXLnglat;
  //       let p2 = p1.offset(100, 0);
  //       let p3 = p2.offset(0, 100);
  //       let p4 = p1.offset(0, 100);
  //       path.push(new AMap.Bounds(p1, p3));
  //       currentXLnglat = nextRightLnglat;
  //     } else {
  //       currentXLnglat = leftDownLnglat.offset(0, 100 * (i + 1));
  //       path.push(
  //         new AMap.Bounds(
  //           new AMap.LngLat(path.at(-1).southWest.lng, path.at(-1).southWest.lat).offset(100, 0),
  //           new AMap.LngLat(path.at(-1).northEast.lng, path.at(-1).northEast.lat).offset(100, 0)
  //         )
  //       );
  //       break;
  //     }
  //   }
  // }

  // let i = 0;
  // while (true) {
  //   if (currentXLnglat.lat > rightUpLnglat.lat) {
  //     break;
  //   }
  //   i++;
  //   while (true) {
  //     let nextRightLnglat = currentXLnglat.offset(100, 0);
  //     if (nextRightLnglat.lng <= rightUpLnglat.lng) {
  //       let p1 = currentXLnglat;
  //       let p2 = p1.offset(100, 0);
  //       let p3 = p2.offset(0, 100);
  //       path.push(new AMap.Bounds(p3, p1));
  //       currentXLnglat = nextRightLnglat;
  //     } else {
  //       currentXLnglat = leftDownLnglat.offset(0, 100 * i);
  //       path.push(
  //         new AMap.Bounds(
  //           new AMap.LngLat(path.at(-1).southWest.lng, path.at(-1).southWest.lat).offset(100, 0),
  //           new AMap.LngLat(path.at(-1).northEast.lng, path.at(-1).northEast.lat).offset(100, 0)
  //         )
  //       );
  //       break;
  //     }
  //   }
  // }
  // initBounds = new AMap.Bounds(p3, p1)

  // let currentXLnglat = leftDownLnglat;
  // let currentXLnglat = new AMap.LngLat(106.497977, 29.614803);
  // new AMap.LngLat(106.49797771827976, 29.614803915510674),
  // new AMap.LngLat(106.49903337509201, 29.61392061112083);

  const p1 = new AMap.LngLat(106.49797771827976, 29.614803915510674);

  let p2 = new AMap.LngLat(106.49797771827976, 29.614803915510674).offset(100, 0);

  let p3 = p2.offset(0, 100);

  let asf = new AMap.Bounds(p1, p3);

  let rectangle1 = new AMap.Rectangle({
    bounds: asf,
    strokeColor: '#1890ff',
    strokeWeight: 1,
    strokeOpacity: 0.9,
    strokeDasharray: [30, 10],
    strokeStyle: 'solid',
    fillColor: 'red',
    fillOpacity: 0.7,
    cursor: 'pointer',
    zIndex: 50,
    extData: {
      id: Math.random().toString(36).slice(2),
    },
  });

  map.add(rectangle1);

  let i = 0;
  while (true) {
    if (currentXLnglat.lat > rightUpLnglat.lat) {
      break;
    }
    i++;
    while (true) {
      let nextRightLnglat = currentXLnglat.offset(100, 0);
      if (nextRightLnglat.lng <= rightUpLnglat.lng) {
        let p1 = currentXLnglat;
        let p2 = p1.offset(100, 0);
        let p3 = p2.offset(0, 100);
        path.push(new AMap.Bounds(p3, p1));
        currentXLnglat = nextRightLnglat;
      } else {
        currentXLnglat = leftDownLnglat.offset(0, 100 * i);
        path.push(
          new AMap.Bounds(
            new AMap.LngLat(path.at(-1).southWest.lng, path.at(-1).southWest.lat).offset(100, 0),
            new AMap.LngLat(path.at(-1).northEast.lng, path.at(-1).northEast.lat).offset(100, 0)
          )
        );
        break;
      }
    }
  }

  let bounddpath = [];
  for (let index = 0; index < path.length; index++) {
    let rectangle = new AMap.Rectangle({
      bounds: path[index],
      strokeColor: '#1890ff',
      strokeWeight: 1,
      strokeOpacity: 0.9,
      strokeDasharray: [30, 10],
      strokeStyle: 'solid',
      fillColor: 'blue',
      fillOpacity: 0,
      cursor: 'pointer',
      zIndex: 3,
      extData: {
        id: Math.random().toString(36).slice(2),
      },
    });
    bounddpath.push(rectangle);
    rectangle.on('click', function (e) {
      console.log('e-rectangle', e.lnglat.toString(), this.getExtData());
    });
  }
  // console.log('bounddpath ', bounddpath);

  // 106.49797771827976,29.614803915510674,
  // 106.49903337509201,29.61392061112083
  // let rectangle1 = new AMap.Rectangle({
  //   bounds: new AMap.Bounds(
  //     new AMap.LngLat(106.49797771827976, 29.614803915510674),
  //     new AMap.LngLat(106.49903337509201, 29.61392061112083)
  //   ),
  //   strokeColor: '#1890ff',
  //   strokeWeight: 1,
  //   strokeOpacity: 0.9,
  //   strokeDasharray: [30, 10],
  //   strokeStyle: 'solid',
  //   fillColor: 'red',
  //   fillOpacity: 0.7,
  //   cursor: 'pointer',
  //   zIndex: 50,
  //   extData: {
  //     id: Math.random().toString(36).slice(2),
  //   },
  // });

  // let rectangle2 = new AMap.Rectangle({
  //   bounds: new AMap.Bounds(
  //     new AMap.LngLat(106.50107398446778, 29.614768891789858),
  //     new AMap.LngLat(106.50212966677509, 29.613885612978446)
  //   ),

  //   strokeColor: '#1890ff',
  //   strokeWeight: 1,
  //   strokeOpacity: 0.9,
  //   strokeDasharray: [30, 10],
  //   strokeStyle: 'solid',
  //   fillColor: 'red',
  //   fillOpacity: 0.7,
  //   cursor: 'pointer',
  //   zIndex: 50,
  //   extData: {
  //     id: Math.random().toString(36).slice(2),
  //   },
  // });
  // let rectangle3 = new AMap.Rectangle({
  //   bounds: new AMap.Bounds(
  //     new AMap.LngLat(106.50418363768387, 29.61563563602729),
  //     new AMap.LngLat(106.50523935536, 29.614752383355057)
  //   ),

  //   strokeColor: '#1890ff',
  //   strokeWeight: 1,
  //   strokeOpacity: 0.9,
  //   strokeDasharray: [30, 10],
  //   strokeStyle: 'solid',
  //   fillColor: 'red',
  //   fillOpacity: 0.7,
  //   cursor: 'pointer',
  //   zIndex: 50,
  //   extData: {
  //     id: Math.random().toString(36).slice(2),
  //   },
  // });

  // let rectangle4 = new AMap.Rectangle({

  //   bounds: new AMap.Bounds(
  //     new AMap.LngLat(106.49694562833633, 29.614815574036705),
  //     new AMap.LngLat(106.49800127664933, 29.613932261121274)
  //   ),

  //   strokeColor: '#1890ff',
  //   strokeWeight: 1,
  //   strokeOpacity: 0.9,
  //   strokeDasharray: [30, 10],
  //   strokeStyle: 'solid',
  //   fillColor: 'red',
  //   fillOpacity: 0.7,
  //   cursor: 'pointer',
  //   zIndex: 50,
  //   extData: {
  //     id: Math.random().toString(36).slice(2),
  //   },
  // });

  map.add(rectangle1);
  // map.add(rectangle2);
  // map.add(rectangle3);
  // map.add(rectangle4);

  map.add(bounddpath);
}

// getViewPath(map);

// map.on('click', function (e) {
//   console.log('map-click', e);
// });

map.on('moveend', function () {
  // console.log('moveend ');
  map.clearMap();

  let rectangle1 = new AMap.Rectangle({
    bounds: new AMap.Bounds(
      new AMap.LngLat(106.49797771827976, 29.614803915510674),
      new AMap.LngLat(106.49903337509201, 29.61392061112083)
    ),
    strokeColor: '#1890ff',
    strokeWeight: 1,
    strokeOpacity: 0.9,
    strokeDasharray: [30, 10],
    strokeStyle: 'solid',
    fillColor: 'red',
    fillOpacity: 0.7,
    cursor: 'pointer',
    zIndex: 50,
    extData: {
      id: Math.random().toString(36).slice(2),
    },
  });

  let rectangle2 = new AMap.Rectangle({
    bounds: new AMap.Bounds(
      new AMap.LngLat(106.50107398446778, 29.614768891789858),
      new AMap.LngLat(106.50212966677509, 29.613885612978446)
    ),

    strokeColor: '#1890ff',
    strokeWeight: 1,
    strokeOpacity: 0.9,
    strokeDasharray: [30, 10],
    strokeStyle: 'solid',
    fillColor: 'red',
    fillOpacity: 0.7,
    cursor: 'pointer',
    zIndex: 50,
    extData: {
      id: Math.random().toString(36).slice(2),
    },
  });
  let rectangle3 = new AMap.Rectangle({
    bounds: new AMap.Bounds(
      new AMap.LngLat(106.50418363768387, 29.61563563602729),
      new AMap.LngLat(106.50523935536, 29.614752383355057)
    ),

    strokeColor: '#1890ff',
    strokeWeight: 1,
    strokeOpacity: 0.9,
    strokeDasharray: [30, 10],
    strokeStyle: 'solid',
    fillColor: 'red',
    fillOpacity: 0.7,
    cursor: 'pointer',
    zIndex: 50,
    extData: {
      id: Math.random().toString(36).slice(2),
    },
  });

  let rectangle4 = new AMap.Rectangle({
    bounds: new AMap.Bounds(
      new AMap.LngLat(106.49694562833633, 29.614815574036705),
      new AMap.LngLat(106.49800127664933, 29.613932261121274)
    ),

    strokeColor: '#1890ff',
    strokeWeight: 1,
    strokeOpacity: 0.9,
    strokeDasharray: [30, 10],
    strokeStyle: 'solid',
    fillColor: 'red',
    fillOpacity: 0.7,
    cursor: 'pointer',
    zIndex: 50,
    extData: {
      id: Math.random().toString(36).slice(2),
    },
  });

  console.log(' map.getZoom()', map.getZoom());
  if (map.getZoom() >= 17) {
    Promise.resolve().then(() => {
      getViewPath(map);
      // map.add(rectangle1);
      // map.add(rectangle2);
      // map.add(rectangle3);
      // map.add(rectangle4);
    });
  }
});

// 麒麟座A
// const qilin = new AMap.LngLat(106.498772, 29.614745);
// const left100 = qilin.offset(0, -100);
// console.log('左边', qilin.distance(left100));

// // 麒麟座A
// {
//   lat:29.614764,
//   lng:106.498774
// }

// // 凤凰A
// {
//   lat:29.614464,
//   lng:106.501404
// }

// // 万国汇
// {
//   lat:29.615005,
//   lng:106.505192
// }

// 麒麟座B
// {
//   lat:29.614747
//   lng:106.497882
// }

const newQILIN = transformFromGCJToWGS(29.614764, 106.498774);
// console.log('newQILIN', newQILIN);
// [106.49797771827976, 29.614803915510674][106.49903337509201, 29.61392061112083]

// [106.50107398446778,29.614768891789858][106.50212966677509,29.613885612978446]

// [106.50418363768387,29.61563563602729][106.50523935536,29.614752383355057]

// [106.49694562833633,29.614815574036705][106.49800127664933,29.613932261121274]
