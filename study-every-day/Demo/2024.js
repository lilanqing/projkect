
function findShortestPath(graph, source) {
  const distances = {};
  const visited = new Set();
  Object.keys(graph).forEach(node => {
    distances[node] = Infinity;
  });
  distances[source] = 0;
  while (Object.keys(visited).length < Object.keys(graph).length) {
    let currentNode = null;
    let shortestDistance = Infinity;
    for (let node in distances) {
      if (!visited.has(node) && distances[node] < shortestDistance) {
        shortestDistance = distances[node];
        currentNode = node;
      }
    }
    for (let edge of graph[currentNode]) {
      const alt = distances[currentNode] + edge.weight;
      if (alt < distances[edge.to]) {
        distances[edge.to] = alt;
      }
    }
    visited.add(currentNode);
  }
  return distances;
}





class Solution2 {
  longestCommonPrefix(strs) {
    if (strs === null || strs.length === 0) {
      return "";
    }
    let prefix = strs[0];
    let count = strs.length;
    for (let i = 1; i < count; i++) {
      prefix = this.longestCommonPrefix(prefix, strs[i]);
      if (prefix.length === 0) {
        break;
      }
    }
    return prefix;
  }

  // longestCommonPrefix(str1, str2) {
  //   let length = Math.min(str1.length, str2.length);
  //   let index = 0;
  //   while (index < length && str1.charAt(index) === str2.charAt(index)) {
  //     index++;
  //   }
  //   return str1.substring(0, index);
  // }
}

// 使用示例
const solution = new Solution2();
console.log(solution.longestCommonPrefix(["flower", "flow", "flight"])); // 输出 "fl"


class Solution3 {
  convertToTitle(columnNumber) {
    let sb = [];
    while (columnNumber !== 0) {
      columnNumber--;
      sb.push(String.fromCharCode(columnNumber % 26 + 'A'.charCodeAt(0)));
      columnNumber = Math.floor(columnNumber / 26);
    }
    return sb.reverse().join('');
  }
}




function findCommonPrefix(strs) {
  if (strs.length === 0) return "";

  let prefix = strs[0];

  for (let i = 1; i < strs.length; i++) {
    while (strs[i].indexOf(prefix) !== 0) {
      prefix = prefix.substring(0, prefix.length - 1);
      if (prefix.length === 0) return "";
    }
  }

  return prefix;
}

// 示例 1
console.log(findCommonPrefix(["flower", "flow", "flight"])); // 输出: "fl"