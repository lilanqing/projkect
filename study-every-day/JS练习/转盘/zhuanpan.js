const lotteryDom = document.querySelector('.lottery');
const btnDom = document.querySelector('.btn');
// let circle = 5; // 旋转圈数
// let cricleCount = 1; // 第几次抽奖
// let rotateAngle = 0; // 旋转角度
// let isrun = false; // 是否运行
// 'https://zos.alipayobjects.com/rmsportal/jkjgkEfvpUPVyRjUImniVslZfWPnJuuZ.png'
class Circle {
  cricleCount = 1;
  rotateAngle = 0;
  isrun = false;
  constructor(lotteryDom, btnDom, circle, drawIndex, time) {
    this.lotteryDom = lotteryDom;
    this.btnDom = btnDom;
    this.circle = circle;
    this.drawIndex = drawIndex;
    this.time = time;
  }
}

class LuckyCircle extends Circle {
  // constructor() {
  //   super();
  // }
  _instance = null;
  static get InstanceLC() {
    if (!this._instance) {
      return (this._instance = new LuckyCircle(lotteryDom, btnDom, 5, Math.floor(Math.random() * 9), 4));
    }
    return this._instance;
  }
  run() {
    const randomIndex = Math.floor(Math.random() * 9);
    this.rotateAngle = 360 * this.circle * this.cricleCount + (randomIndex * 45 + 22.5);
    // this.lotteryDom.style.transition = `all  ease-in-out`;
    this.lotteryDom.style.transition = `all ${this.time}s cubic-bezier(0.45, 0.05, 0.69, 1.41)`;

    this.lotteryDom.style.transform = `rotate(${this.rotateAngle}deg)`;

    setTimeout(() => {
      alert(`当前随机索引${randomIndex}-当前旋转${this.rotateAngle - 360 * this.circle * this.cricleCount++}度`);
    }, +(this.time + '000'));
  }
}
btnDom.onclick = function (ev) {
  LuckyCircle.InstanceLC.run();
};
