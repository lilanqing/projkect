// console.log(aa);
// function name1(params) {
//   for (let i = 0; i < 3; i++) {
//     setTimeout(() => {
//       console.log(i);
//     }, i*100);
//   }
// }
// name1()

function a() {
  console.log(1);
  setTimeout(() => {
    console.log(2);
  }, 1000);

  setTimeout(() => {
    console.log(3);
  }, 0);

  new Promise(function (resolve) {
    setTimeout(() => {
      resolve();
      console.log(8);
    }, 0);
    console.log(7);
  }).then(function () {
    console.log(4);
  });
  console.log(5);
}
// a(); // a()1 8 7 5 4 3 2

//查找关键词
let arr1 = ['山感受', '感受', '发顺丰'];
let str = '个人股发顺畅狗日封山年花果山感受';
console.log(arr1.some((item) => str.includes(item)));
console.log(arr1.find((item) => str.includes(item)));
console.log(arr1.every((item) => str.indexOf(item) >= 0));

// function findStr(arr = arr1, str = '个人股发顺畅狗日封山年花果山感受') {
//   let flag = arr.some(item => { return str.includes(item) })
//   flag && console.log('找到了');
//   // flag ? console.log('找到了') : console.log('没找到了');
// }
// // flag && console.log('找到了');
// findStr()

// let aaa = String(123456).slice(0, -2) + '*'.repeat(2)
// console.log(aaa);

// let arr = [1, 2, 3, 4, 5]
// let a = arr.reduce((prv, cur) => {
//   return prv += cur
// }, 0)
// console.log(a);
// let arr = ['1', '2', '3', '4']
// console.log(...arr);
// console.log(String(arr).split(','));
// console.log(arr.toString());
// // console.log(arr.map(Number));
// arr.map(String)

// let arr = ['29.123', '106.123']
// let a = [arr.map(Number)[1], arr.map(Number)[0]]
// let b = arr.map(Number).reverse()
// let c = `${arr.map(Number)[1]},
// ${arr.map(Number)[0]}`
// console.log(a, b, c);
// /^\s|\s$/

// function sleep() {
//   return new Promise((resolve, reject) => {
//     setTimeout(resolve, 1000);
//   });
// }

// async function itrea() {
//   for (let i = 0; i <= 3; i++) {
//     // setTimeout(function () {
//     //   console.log(i);
//     // }, 1000 * i);
//     await sleep();
//     console.log(i);
//   }
// }

// itrea();

// console.log(obj.hasOwnProperty('length'));// 检车对象本身上是否含有该属性
// console.log(obj.hasOwnProperty('name'));
Object.hasOwn(obj, 'NAME');
// console.log("name" in obj);
// let b = {
//   length: 123
// }
// Object.setPrototypeOf(obj, b)// 为某个对象重新设置原型对象
// Object.setOwnPropertyDesc(obj,'name')//查看对象每个属性的特征
// Object.defineProperty()// 配置属性特征
// Object.seal()进行对象封闭 isSealed判断对象是否封闭
// Object.freeze()冻结对象

let js = {
  sing() { },
  name: null,
  age: 123,
  sd: undefined,
  time: new Date(),
};
//深拷贝
console.log(JSON.parse(JSON.stringify(js)));
//深拷贝
// function deepCopy1(newobj, oldobj) {
//   for (let [k, v] of Object.entries(oldobj)) {
//     switch (Object.prototype.toString.call().slice(1, -8)) {
//       case 'array':
//         newobj[k] = [];
//         deepCopy1(newobj[k], v);
//         break;
//       case 'object':
//         newobj[k] = {};
//         deepCopy1(newobj[k], v);
//         break;
//       default:
//         newobj[k] = v;
//         break;
//     }
//   }
// }
//深拷贝
// function copy(obj) {
//   let res = obj instanceof Array ? [] : {};
//   for (let [v, k] of Object.entries(obj)) res[v] = typeof k == 'object' ? copy(k) : k;
//   return res;
// }

// const copy = (obj) => {
//   let val = obj instanceof Array ? [] : {};
//   for (let k in obj) val[k] = typeof obj[k] == 'object' ? copy(obj[k]) : obj[k];
//   return val;
// };
// 深拷贝拷贝对象封装函数 (递归)
// function deepCopy(newobj, oldobj) {
//   for (var k in oldobj) {
//     // 判断我们的属性值属于那种数据类型
//     // 1. 获取属性值  oldobj[k]
//     var item = oldobj[k];
//     // 2. 判断这个值是否是数组
//     if (item instanceof Array) {
//       newobj[k] = [];
//       deepCopy(newobj[k], item);
//     } else if (item instanceof Object) {
//       // 3. 判断这个值是否是对象
//       newobj[k] = {};
//       deepCopy(newobj[k], item);
//     } else {
//       // 4. 属于简单数据类型
//       newobj[k] = item;
//     }
//   }
// }

const lessons = [
  {
    title: '媒体查询响应式布局',
    category: 'css',
  },
  {
    title: 'FLEX 弹性盒模型',
    category: 'css',
  },
  {
    title: 'MYSQL多表查询随意操作',
    category: 'mysql',
  },
];
let lessonObj = lessons.reduce((obj, cur, index) => {
  obj[`${cur['category']}-${index}`] = cur;
  return obj;
}, {});

console.log(lessonObj); //{css-0: {…}, css-1: {…}, mysql-2: {…}}
// console.log(lessonObj["css-0"]); //{title: "媒体查询响应式布局", category: "css"}

// setTimeout(() => {
//   console.log('setTimeout');
// });

// setImmediate(function () {
//   console.log('setImmediate');
// });
