let intervalId = null;
lastTime = 0;
const qiuEl = document.querySelector('.qiu');
qiuEl.onclick = function () {
  console.log('qiuEl');
  // window.open('www.baidu.com');
  location.port = 'www.baidu.com';
};
// requestAnimationFrame 会把每一帧中的所有DOM操作集中起来，在一次重绘或回流中就完成，并且重绘或回流的时间间隔紧紧跟随浏览器的刷新频率，一般来说，这个频率为每秒60帧。
// requestAnimationFrame是在渲染主线程上完成;

let x = 0;
const run = function () {
  requestAnimationFrame((curTime) => {
    // curTime 调用回调函数时的时间
    if (curTime - lastTime > 1000) {
      console.log(' 调用回调函数时的时间', curTime - lastTime);
      console.log(x);
      lastTime = curTime;
      x += 20;
      if (x >= 1000) x = 0;
      qiuEl.style.transform = 'translateX(' + x + 'px)';
      run();
    } else {
      run();
    }
  });
};
// run();

(function run2() {
  requestAnimationFrame((curTime) => {
    x += 1;
    if (x >= 1000) x = 0;
    qiuEl.style.transform = 'translateX(' + x + 'px)';

    if (x % 22 !== 0) {
      run2();
    } else {
      setTimeout(() => {
        run2();
      }, 1000);
    }
  });
})();

// (function animloop() {
//   //记录当前时间
//   nowTime = Date.now()
//   // 当前时间-上次执行时间如果大于diffTime，那么执行动画，并更新上次执行时间
//   if(nowTime-lastTime > diffTime){
//       lastTime = nowTime
//       render();
//   }
//   requestAnimationFrame(animloop);

// })()

/**
 * 原生走马灯
 */

let canIndex = 0;
function moveTo(index) {
  document.querySelector('.caul').style.transition = '0.5s';
  document.querySelector('.caul').style.transform = 'translateX(-' + index + '00%)';
  document.querySelector('.dian>span.active')?.classList.remove('active');
  document.querySelectorAll('.dian>span')[index]?.classList.add('active');
  canIndex = index;
}
document.querySelectorAll('.dian>span').forEach((item, index) => {
  item.onclick = function () {
    moveTo(index);
  };
});

function always() {
  intervalId = setInterval(() => {
    if (canIndex === document.querySelectorAll('.dian>span').length - 1) {
      document.querySelector('.caul').style.transform = 'translateX(100%)';
      document.querySelector('.caul').style.transition = 'none';
      document.querySelector('.caul').clientHeight;
      moveTo(0);
    } else {
      moveTo(canIndex + 1);
    }
  }, 3000);
}

document.querySelector('.left').onclick = function () {
  console.log('left');
  if (canIndex === 0) {
    document.querySelector('.caul').style.transform =
      'translateX(-' + document.querySelectorAll('.dian>span').length + '00%)';
    document.querySelector('.caul').style.transition = 'none';
    document.querySelector('.caul').clientHeight;
    moveTo(document.querySelectorAll('.dian>span').length - 1);
    return;
  }
  moveTo(canIndex - 1);
};
document.querySelector('.right').onclick = function () {
  console.log('right');
  if (canIndex === document.querySelectorAll('.dian>span').length - 1) {
    document.querySelector('.caul').style.transform = 'translateX(100%)';
    document.querySelector('.caul').style.transition = 'none';
    document.querySelector('.caul').clientHeight;
    moveTo(0);
    return;
  }
  moveTo(canIndex + 1);
};
always();

/**
 * 初始化
 */
function canulInitiate() {
  const cauls = document.querySelector('.caul');
  // console.dir(cauls.getBoundingClientRect());
  // console.log(document.importNode(document.querySelector('.caul').firstElementChild, true));
  // console.dir(cauls.lastElementChild.cloneNode(true));
  const firstEl = document.importNode(cauls.firstElementChild, true);
  const lastEl = cauls.lastElementChild.cloneNode(true);
  cauls.appendChild(firstEl);
  const newF = cauls.insertBefore(lastEl, cauls.firstElementChild);
  newF.style.cssText = 'position: absolute;transform: translateX(-100%);';
}
canulInitiate();
