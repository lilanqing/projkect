// importScripts('script1.js');

function onmessage(event) {
  console.log('接收到主线程的消息:', event.data);
  self.postMessage('You said: ' + event.data);
  self.close();
}
self.addEventListener('message', onmessage, false);

// 这个技术，在多个js模块进行数据交互时用起来很方便，比如在websocke通信的onmessage方法中将服务器响应的数据，回调到其它js模块，用这种手法就很爽。
// 目前chrome，nodejs都已支持worker，在worker创建时，第二个参数type 可以用module，这样线程中导入脚本，可以直接使用 import 或require. 线程使用中需要注意，postmessage不能高频率调用，会导致cpu和内存剧增

// =====================在react中使用web-work============================================

// const workercode = () => {
//   self.onmessage = function (e) {
//     console.log('Message received from main script');
//     let workerResult = `Received from main: ${e.data}`;
//     console.log('Posting message back to main script');
//     self.postMessage(workerResult);
//   };

//   self.postMessage('workerResult');
// };

// let code = workercode.toString();
// code = code.substring(code.indexOf('{') + 1, code.lastIndexOf('}'));

// const blob = new Blob([code], { type: 'application/javascript' });
// const worker_script = URL.createObjectURL(blob);

// export default worker_script;

// webworker创建有两种方式，一种是引入外部js,还有一种是用就是脚本字符串的方式

//1. new Worker('./worker.js')

//2. var blob = new Blob([document.querySelector('#worker').textContent]); // textContent为字符串脚本
// var url = window.URL.createObjectURL(blob);
// var worker = new Worker(url);
