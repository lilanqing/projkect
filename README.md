### 好看的颜色网站

https://flatuicolors.com/

### 免费图片网站

- https://api.thecatapi.com/v1/images/search?limit=10
- https://api.thecatapi.com/v1/images/search
- https://images.unsplash.com/
- https://source.unsplash.com/
- https://picsum.photos/

`生成树状目录结构：tree /f > txt.txt`

```
│  App.vue
│  main.ts
│  shims-vue.d.ts
│
├─assets
│  │  logo.png
│  │
│  └─css
│          element-variables.scss
│          global.scss
│
├─components
│      Base.vue
│      Buttom.vue
│      LoginS.vue
│      Main.vue
│      News.vue
│      News2.vue
│      NotFound.vue
│      Top.vue
│
├─router
│      index.ts
│
├─store
│      index.ts
│
└─views
        About.vue
        Dynamic.vue
        Echarts.vue
        ElementUiPlus.vue
        Home.vue
        Login.vue
        Request.vue
        Transition.vue
        User.vue

```

```
'type-enum': [2, 'always', ['feat', 'fix', 'docs', 'style', 'refactor', 'test', 'revert', 'perf', 'chore'],

"lint-staged": {
"src/**/\*.less": "stylelint --syntax less",
"src/**/\_.{js,jsx,ts,tsx}": "npm run lint-staged:js",
"\*\*/\*.{jsx,tsx,ts,md,json}": [
"prettier --write"
]
}


"lint-staged:js": "eslint --ext .js,.jsx,.ts,.tsx ",
"lint:fix": "eslint --fix --cache --ext .js,.jsx,.ts,.tsx --format=pretty ./src && npm run lint:style",
"lint:js": "eslint --cache --ext .js,.jsx,.ts,.tsx --format=pretty ./src",
"lint:prettier": "prettier --check \"src/\*\*/_\" --end-of-line auto",
"lint:style": "stylelint --fix \"src/\*\*/_.less\" --syntax less",
"precommit": "lint-staged",
"prepare": "husky install",
"postinstall": "husky install",
"prettier": "prettier -c --write \"src/\*_/_\""
```

target 用于指定编译之后的版本目录

"target": "es5",  
module 用来指定要使用的模板标准

```js
"module": "commonjs",
lib 用于指定要包含在编译中的库文件
"lib":[
"es6",
"dom"
],

allowJs 用来指定是否允许编译 JS 文件，默认 false,即不编译 JS 文件
"allowJs": true,

checkJs 用来指定是否检查和报告 JS 文件中的错误，默认 false
"checkJs": true,

指定 jsx 代码用于的开发环境:‘preserve’,‘react-native’,or 'react
"jsx": "preserve",

declaration 用来指定是否在编译的时候生成相的 d.ts 声明文件，如果设为 true,编译每个 ts 文件之后会生成一个 js 文件和一个声明文件，但是 declaration 和 allowJs 不能同时设为 true
"declaration": true,

declarationMap 用来指定编译时是否生成.map 文件
"declarationMap": true,

socuceMap 用来指定编译时是否生成.map 文件
"sourceMap": true,

outFile 用于指定输出文件合并为一个文件，只有设置 module 的值为 amd 和 system 模块时才支持这个配置
"outFile": "./",

outDir 用来指定输出文件夹，值为一个文件夹路径字符串，输出的文件都将放置在这个文件夹
"outDir": "./",

rootDir 用来指定编译文件的根目录，编译器会在根目录查找入口文件
"rootDir": "./",

composite 是否编译构建引用项目
"composite": true,

removeComments 用于指定是否将编译后的文件注释删掉，设为 true 的话即删除注释，默认为 false
"removeComments": true,

noEmit 不生成编译文件
"noEmit": true,

importHelpers 指定是否引入 tslib 里的复制工具函数，默认为 false
"importHelpers": true,

当 target 为"ES5"或"ES3"时，为"for-of" "spread"和"destructuring"中的迭代器提供完全支持
"downlevelIteration": true,

isolatedModules 指定是否将每个文件作为单独的模块，默认为 true，他不可以和 declaration 同时设定
"isolatedModules": true,

strict 用于指定是否启动所有类型检查，如果设为 true 这回同时开启下面这几个严格检查，默认为 false
"strict": true,

noImplicitAny 如果我们没有一些值设置明确类型，编译器会默认认为这个值为 any 类型，如果将 noImplicitAny 设为 true,则如果没有设置明确的类型会报错，默认值为 false
"noImplicitAny": true,

strictNullChecks 当设为 true 时，null 和 undefined 值不能赋值给非这两种类型的值，别的类型的值也不能赋给他们，除了 any 类型，还有个例外就是 undefined 可以赋值给 void 类型
"strictNullChecks": true,

strictFunctionTypes 用来指定是否使用函数参数双向协变检查
"strictFunctionTypes": true,

strictBindCallApply 设为 true 后对 bind、call 和 apply 绑定的方法的参数的检测是严格检测
"strictBindCallApply": true,

strictPropertyInitialization 设为 true 后会检查类的非 undefined 属性是否已经在构造函数里初始化，如果要开启这项，需要同时开启 strictNullChecks,默认为 false
"strictPropertyInitialization": true,

当 this 表达式的值为 any 类型的时候，生成一个错误
"noImplicitThis": true,

alwaysStrict 指定始终以严格模式检查每个模块，并且在编译之后的 JS 文件中加入"use strict"字符串，用来告诉浏览器该 JS 为严格模式
"alwaysStrict": true,

noUnusedLocals 用于检查是否有定义了但是没有使用变量，对于这一点的检测，使用 ESLint 可以在你书写代码的时候做提示，你可以配合使用，他的默认值为 false
"noUnusedLocals": true,

noUnusedParameters 用于检测是否在函数中没有使用的参数
"noUnusedParameters": true,

noImplicitReturns 用于检查函数是否有返回值，设为 true 后，如果函数没有返回值则会提示，默认为 false
"noImplicitReturns": true,

noFallthroughCasesInSwitch 用于检查 switch 中是否有 case 没有使用 break 跳出 switch,默认为 false
"noFallthroughCasesInSwitch": true,

moduleResolution 用于选择模块解析策略，有"node"和"classic"两种类型
"moduleResolution": "node",

baseUrl 用于设置解析非相对模块名称的基本目录，相对模块不会受到 baseUrl 的影响
"baseUrl": "./",

paths 用于设置模块名到基于 baseUrl 的路径映射
"paths": {
"_":["./node_modules/@types", "./typings/_"]
},

rootDirs 可以指定一个路径列表，在构建时编译器会将这个路径中的内容都放到一个文件夹中
"rootDirs": [],

typeRoots 用来指定声明文件或文件夹的路径列表，如果指定了此项，则只有在这里列出的声明文件才会被加载
"typeRoots": [],

types 用于指定需要包含的模块，只有在这里列出的模块的声明文件才会被加载
"types": [],



allowSyntheticDefaultImports 用来指定允许从没有默认导出的模块中默认导入
"allowSyntheticDefaultImports": true,

esModuleInterop 通过导入内容创建命名空间，实现 CommonJS 和 ES 模块之间的互操作性
"esModuleInterop": true,

不把符号链接解析为真实路径，具体可以了解下 webpack 和 node.js 的 symlink 相关知识
"preserveSymlinks": true,

sourceRoot 用于指定调试器应该找到 TypeScript 文件而不是源文件的位置，这个值会被写进.map 文件里
"sourceRoot": "",

mapRoot 用于指定调试器找到映射文件而非生成文件的位置，指定 map 文件的根路径，该选项会影响.map 文件中的 sources 属性
"mapRoot": "",

inlineSourceMap 指定是否将 map 文件内容和 js 文件编译在一个同一个 js 文件中，如果设为 true,则 map 的内容会以//#soureMappingURL=开头，然后接 base64 字符串的形式插入在 js 文件底部
"inlineSourceMap": true,

inlineSources 用于指定是否进一步将 ts 文件的内容也包含到输出文件中
"inlineSources": true,

experimentalDecorators 用于指定是否启用实验性的装饰器特性
"experimentalDecorators": true,

emitDecoratorMetadata 用于指定是否为装上去提供元数据支持，关于元数据，也是 ES6 的新标准，可以通过 Reflect 提供的静态方法获取元数据，如果需要使用 Reflect 的一些方法，需要引用 ES2015.Reflect 这个库
"emitDecoratorMetadata": true,

include 也可以指定要编译的路径列表
"include":[],

files 可以配置一个数组列表
"files":[],

exclude 表示要排除的，不编译的文件，它也可以指定一个列表，规则和 include 一样，可以是文件可以是文件夹，可以是相对路径或绝对路径，可以使用通配符
"exclude":[]

extends 可以通过指定一个其他的 tsconfig.json 文件路径，来继承这个配置文件里的配置，继承来的文件的配置会覆盖当前文件定义的配置
"extends":""

compileOnSave 如果设为 true,在我们编辑了项目文件保存的时候，编辑器会根据 tsconfig.json 的配置更新重新生成文本，不过这个编辑器支持
"compileOnSave":true

一个对象数组，指定要引用的项目
 "references":[]
```

react-drag-listview

```
X-Frame-Options
content-security-policy
httpOnly
secure
sameSite
interaction
actions
operations
```
