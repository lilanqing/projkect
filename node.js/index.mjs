// import path from 'node:path';
// console.log('==process.argv====>', process.argv)
// console.log('==path====>', path.resolve()) 
// 请求报文
//   请求首行
//   请求头
//   空行
//   请求体


// 响应报文
//   响应首行
//   响应头
//   空行
//   响应体




function extractOptionsFromJSDoc(jsDoc) {
  const options = {};
  const regex = /@(\w+)\s+(.+)/g;
  let match;
  while ((match = regex.exec(jsDoc)) !== null) {
    const key = match[1];
    const value = match[2].trim();
    options[key] = value;
  }
  return options;
}

// 示例 JSDoc 注释
const jsDoc = `
/**
* 这是一个示例函数
* @param {string} name - 名称
* @param {number} age - 年龄
* @param {boolean} isActive - 是否激活
*/
`;

const options = extractOptionsFromJSDoc(jsDoc);
console.log(options);


// console.log('__dirname', __dirname);
// console.log('__filename', __filename);
// console.log('process.cwd', process.cwd());
// console.log('path.resolve', path.resolve());
// console.log('path.join', path.join());
// console.log(import.meta.url);



// try {
//   a
//   throw new Error('error')

// } finally {
//   console.log('==  finally ====>',)
// }
