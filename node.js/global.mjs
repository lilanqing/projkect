// import http from 'http';
// import fs from 'node:fs/promises';
// const fs = require('fs');
// const http = require('http');
// const path = require('path');
// const os = require('node:os')
// const https = require('node:https');
// const workerThreads = require('node:worker_threads');
// const path = require('path');

// module.exports = exports;
// path.extname(); //返回文件后缀名
// const querystring = require('querystring');
// let str = 'api/bin/index.html';
// const myURL = new URL('https://example.org?id=1&name=llq');
// console.log(myURL);
// console.log(path.basename(str));
// console.log(path.extname(str));
// const aaa = 'id=1&name=llq';
// console.log(querystring.parse(aaa));
// console.log(myURL);
// __filename 表示当前正在执行的脚本的文件名 (包含目录)
// __dirname 表示当前执行脚本所在的目录。
// console.log('__filename', __filename);
// console.log('__dirname', __dirname);

// console.log(path.join(__dirname, '../aaa', '/api'));

// console.log(path.join(__dirname, './'));
//路径拼接
// path.basename 路径中的文件名 最后一个/之后
// path.extname 文件名中的扩展名
// path.normalize()整理不整齐的路径成标准路径
// console.log(path.normalize('/users/joe/..//test.txt'));//'/users/test.txt'

// pacejosn中 ^是锁定第一个 ~是锁定第二个

//path.join ，path.resolve都是连接文件目录地址
// console.log(path.join(__dirname, '../api'));
// console.log(path.resolve(__dirname, '../'));
// node.js中 默认this是{}  console.log(this);
// let b1 = Buffer.alloc(10);
// console.log(b1);
// // let buf = Buffer.from('1');接受 字符串 数组 buffer
// let buf1 = Buffer.from([1, 2, 3]);
// console.log(buf1);
// process.env.NODE_ENV = 'dev';
// console.log(process.argv[1]);
// console.log(__filename);
// console.log(__dirname);


// console.log('process', process);

// console.log(path.join(__dirname, 'api'));

// console.log(path.resolve(__dirname, 'api'));



// console.log(path.resolve());
// console.log(path.resolve(__dirname, 'tmp/file'));
// console.log(path.join(__dirname, '/tmp/file'));


// console.log(require('os').networkInterfaces())
// console.log(path.resolve(__dirname, 'tmp/file'));

// console.log(path.resolve('wwwroot', 'static_files/png', 'gif/image.gif'));
// console.log(path.join('wwwroot', 'static_files/png/', 'gif/image.gif'));

// console.log(path.resolve('foo', 'bar', 'baz'));
// console.log(module.exports == exports);
// console.log(exports);
// console.log(module);

// console.log(process.nextTick);
// console.log(path.normalize('/users/joe/..//test.txt'));
//'/users/test.txt'
// console.log(path.resolve('data.txt'));
// fs.readFile(path.join(__dirname, 'data.txt'), 'utf-8', (err, data) => {
//   if (err) {
//     return;
//   }
//   console.log(data + '12313123');
// });
// fs.mkdir(path.join(__dirname, 'wenjian'), { recursive: true }, (err) => {
//   console.log('123231');
// });
// fs.mkdir(path.join(__dirname, 'wenjian'), { recursive: true }, (err) => {
//   console.log('12');
// });

// fs.open(path.join(__dirname, 'data.txt'), 'r', (err, data) => {
//   if (err) {
//     return;
//   }
//   console.log(data + '11');
// });

// setImmediate(() => {
//   console.log('setImmediate-5');
// });


// process.nextTick(() => {
//   console.log('nextTick-2');
// });
// setTimeout(() => {
//   console.log('setTimeout0-4');
// }, 0);
// setTimeout(() => {
//   console.log('setTimeou10-6');
// }, 10);

// console.log('主线程-1', Object.keys(globalThis));
// console.log('process.cwd()', process.cwd());
// console.log('import.meta', import.meta.url);
// new FormData();

// console.log('==this====>', this)

setTimeout(() => {
  console.log('setTimeout');
}, 0);
setImmediate(() => {
  console.log('setImmediate');
});

process.nextTick(() => {
  console.log('nextTick');
});

Promise.resolve().then(() => {
  console.log('Promise resolve-3');
});

