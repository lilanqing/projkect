const fs = require('node:fs');

const readStream = fs.createReadStream('./1.txt', 'utf-8');

readStream.once('open', () => {
  console.log('==creadStream-open====>');
})
readStream.once('close', () => {
  console.log('==creadStream-open====>');
})

readStream.on('data', (chunked) => {
  console.log('==creadStream-chunked====>', chunked);
});
readStream.on('end', () => {
  console.log('== end ====>');
});





const writeStream = fs.createWriteStream('./2.txt', 'utf-8');
writeStream.write('123');
writeStream.write('hello world');
writeStream.end('end');
// 监听写流的打开与关闭
writeStream.once('open', () => {

})
writeStream.once('close', () => {

})