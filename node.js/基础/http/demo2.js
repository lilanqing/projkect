const cheerio = require('cheerio');

let url = 'http://www.baidu.com';

http
  .get(url, (res) => {
    const { statusCode } = res;
    const contentType = res.headers['content-type'];
    let error;
    if (statusCode !== 200) {
      error = new Error('Request Failed.\n' + `Status Code: ${statusCode}`);
    } else if (!/^text\/html/.test(contentType)) {
      error = new Error(
        'Invalid content-type.\n' + `Expected application/json but received ${contentType}`
      );
    }
    if (error) {
      console.error(error.message);
      res.resume();
      return;
    }
    let resData = '';
    res.on('data', (chunk) => {
      console.log('文件传输');
      resData += chunk;
    });
    res.on('end', (err) => {
      console.log('文件传输完毕');
      fs.writeFileSync(path.join(__dirname, '/baidu.html'), resData);
      //爬取图片
      let $ = cheerio.load(resData);
      $('img').each((i, el) => {
        console.log($(el).attr('src'));
      });
    });
  })
  .on('error', (err) => {
    console.log('请求错误');
  });