
const http = require('node:http')

//创建服务
const service = http.createServer((req, res) => {
  //通过设置头信息进行页面跳转
  // res.statusCode = 301
  // res.setHeader('Location', 'https://www.houdunren.com')

  res.writeHead(200, {
    'Content-Type': 'text/plain;charset=utf8',
    // 'Content-type': 'application/json',
    'Transfer-Encoding': 'chunked',
    'Access-Control-Allow-Origin': '*'
  })



  // 发送数据块
  res.write('5\r\nHello\r\n'); // 发送第一个数据块
  res.write('6\r\nWorld!\r\n'); // 发送第二个数据块
  res.end('0\r\n\r\n'); // 发送最后一个数据块并结束响应
  //响应数据
  // res.end('<h1>后盾人-houdunren.com@向军大叔</h1>');
})

//监听端口
// 如果不设置主机名，或设置为 0.0.0.0 表示允许任何IP 访问，这样局域网其他电脑可以通过 IP 访问到你的项目。
// 设置主机名为 localhost 或 '127.0.0.1' 只允许本机访问
service.listen(8080, () => {
  console.log('HOST: http://localhost:8080')
})
