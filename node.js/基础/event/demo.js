const EventEmitter = require('events')

const event = new EventEmitter()


// on 方法用于订阅监听事件
// emit 方法用于触发事件
// once 订阅一次事件，即执行后删除事件
// removeListener 移除事件
// removeAllListeners 移除所有事件





//监听事件
event.on('hd', (content) => {
  console.log(`第1次触发事件：${content}`)
})

//监听事件
event.on('hd', (content) => {
  console.log(`第2次触发事件：${content}`)
})

console.log('事件驱动是异步的，所以这行代码先执行')

// 触发事件
event.emit('hd', 'houdunren.com')