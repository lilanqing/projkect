Node.js 中文网
关于本文档
用法与示例
assert 断言
async_hooks 异步钩子
async_hooks / context 异步上下文
buffer 缓冲区
C++插件
C / C++插件(使用 Node - API)
C++嵌入器
child_process 子进程
cluster 集群
CLI 命令行
console 控制台
Corepack 核心包
crypto 加密
crypto / webcrypto 网络加密
debugger 调试器
deprecation 弃用
dgram 数据报
diagnostics_channel 诊断通道
dns 域名服务器
domain 域
Error 错误
events 事件触发器
fs 文件系统
global 全局变量
http 超文本传输协议
http2 超文本传输协议 2.0
https 安全超文本传输协议
inspector 检查器
Intl 国际化
module 模块
module / cjs CommonJS 模块
module / esm ECMAScript 模块
module / package 包模块
net 网络
os 操作系统
path 路径
perf_hooks 性能钩子
permission 权限
process 进程
punycode 域名代码
querystring 查询字符串
readline 逐行读取
repl 交互式解释器
report 诊断报告
sea 单个可执行应用程序
stream 流
stream / web 网络流
string_decoder 字符串解码器
test 测试
timers 定时器
tls 安全传输层
trace_events 跟踪事件
tty 终端
url 网址
util 实用工具
v8 引擎
vm 虚拟机
wasi 网络汇编系统接口
worker_threads 工作线程
zlib 压缩
Node.js v20.12.0  目录  导航  其他版本
万维广告联盟
🛒 B2B2C商家入驻平台系统java版 Java + vue + uniapp 功能强大 稳定 支持diy 方便二开

广告
目录
URL
URL 字符串和 URL 对象
从组成部分构造 URL 并获取构造的字符串
WHATWG URL API
类：URL
new URL(input[, base])
url.hash
url.host
url.hostname
url.href
url.origin
url.password
url.pathname
url.port
url.protocol
特别计划
url.search
url.searchParams
url.username
url.toString()
url.toJSON()
URL.createObjectURL(blob)
URL.revokeObjectURL(id)
URL.canParse(input[, base])
类：URLSearchParams
new URLSearchParams()
new URLSearchParams(string)
new URLSearchParams(obj)
new URLSearchParams(iterable)
urlSearchParams.append(name, value)
urlSearchParams.delete(name[, value])
urlSearchParams.entries()
urlSearchParams.forEach(fn[, thisArg])
urlSearchParams.get(name)
urlSearchParams.getAll(name)
urlSearchParams.has(name[, value])
urlSearchParams.keys()
urlSearchParams.set(name, value)
urlSearchParams.size
urlSearchParams.sort()
urlSearchParams.toString()
urlSearchParams.values()
urlSearchParams[Symbol.iterator]()
url.domainToASCII(domain)
url.domainToUnicode(domain)
url.fileURLToPath(url)
url.format(URL[, options])
url.pathToFileURL(path)
url.urlToHttpOptions(url)
旧版 URL API
旧版 urlObject
urlObject.auth
urlObject.hash
urlObject.host
urlObject.hostname
urlObject.href
urlObject.path
urlObject.pathname
urlObject.port
urlObject.protocol
urlObject.query
urlObject.search
urlObject.slashes
url.format(urlObject)
url.parse(urlString[, parseQueryString[, slashesDenoteHost]])
url.resolve(from, to)
URL 中的百分比编码
旧版 API
WHATWG API

URL#
稳定性: 2 - 稳定的
源代码: lib / url.js

node:url 模块提供用于网址处理和解析的实用工具。可以使用以下方式访问它：

const url = require('node:url'); 拷贝

URL 字符串和 URL 对象#
网址字符串是包含多个有意义组件的结构化字符串。解析时，将返回包含每个组件的属性的网址对象。

node:url 模块提供了两个用于处理 URL 的 API：一个特定于 Node.js 的旧版 API，以及一个实现 Web 浏览器使用的相同 WHATWG URL 标准 的较新 API。

下面提供了 WHATWG 和旧版 API 之间的比较。在网址 'https://user:pass@sub.example.com:8080/p/a/t/h?query=string#hash' 上方显示的是由旧版 url.parse() 返回的对象的属性。下方则是 WHATWG URL 对象的属性。

WHATWG 网址的 origin 属性包括 protocol 和 host，但不包括 username 或 password。

┌────────────────────────────────────────────────────────────────────────────────────────────────┐
│                                              href                                              │
├──────────┬──┬─────────────────────┬────────────────────────┬───────────────────────────┬───────┤
│ protocol │  │        auth         │          host          │           path            │ hash  │
│          │  │                     ├─────────────────┬──────┼──────────┬────────────────┤       │
│          │  │                     │    hostname     │ port │ pathname │     search     │       │
│          │  │                     │                 │      │          ├─┬──────────────┤       │
│          │  │                     │                 │      │          │ │    query     │       │
"  https:   //    user   :   pass   @ sub.example.com : 8080   /p/a/t/h  ?  query=string   #hash "
│          │  │          │          │    hostname     │ port │          │                │       │
│          │  │          │          ├─────────────────┴──────┤          │                │       │
│ protocol │  │ username │ password │          host          │          │                │       │
├──────────┴──┼──────────┴──────────┼────────────────────────┤          │                │       │
│   origin    │                     │         origin         │ pathname │     search     │ hash  │
├─────────────┴─────────────────────┴────────────────────────┴──────────┴────────────────┴───────┤
│                                              href                                              │
└────────────────────────────────────────────────────────────────────────────────────────────────┘
(All spaces in the "" line should be ignored.They are purely for formatting.) 拷贝
使用 WHATWG API 解析网址字符串：

const myURL =
  new URL('https://user:pass@sub.example.com:8080/p/a/t/h?query=string#hash'); 拷贝
使用旧版 API 解析 URL 字符串：

const url = require('node:url');
const myURL =
  url.parse('https://user:pass@sub.example.com:8080/p/a/t/h?query=string#hash'); 拷贝

从组成部分构造 URL 并获取构造的字符串#
可以使用属性设置器或模板字面串从组件部分构建 WHATWG 网址：

const myURL = new URL('https://example.org');
myURL.pathname = '/a/b/c';
myURL.search = '?d=e';
myURL.hash = '#fgh'; 拷贝
const pathname = '/a/b/c';
const search = '?d=e';
const hash = '#fgh';
const myURL = new URL(`https://example.org${pathname}${search}${hash}`); 拷贝
要获取构造的网址字符串，则使用 href 属性访问器：

console.log(myURL.href); 拷贝

WHATWG URL API#

类：URL#
版本历史
浏览器兼容的 URL 类，按照 WHATWG 网址标准实现。已解析 URL 的示例 可以在标准本身中找到。URL 类也在全局对象上可用。

按照浏览器的约定，URL 对象的所有属性都被实现为类原型上的获取器和设置器，而不是对象本身的数据属性。因此，与 旧版 urlObject 不同，在 URL 对象（例如 delete myURL.protocol、delete myURL.pathname 等）的任何属性上使用 delete 关键字没有效果，但仍会返回 true。


new URL(input[, base])#
版本历史
input < string > 要解析的绝对或相对的输入网址。如果 input 是相对的，则需要 base。如果 input 是绝对的，则忽略 base。如果 input 不是字符串，则首先是 转换为字符串。

base < string > 如果 input 不是绝对的，则为要解析的基本网址。如果 base 不是字符串，则首先是 转换为字符串。

通过相对于 base 解析 input 来创建新的 URL 对象。如果 base 作为字符串传入，则其将被解析为等效于 new URL(base)。

const myURL = new URL('/foo', 'https://example.org/');
// https://example.org/foo 拷贝
网址构造函数可作为全局对象的属性访问。也可以从内置的 url 模块中导入：

console.log(URL === require('node:url').URL); // Prints 'true'.拷贝
如果 input 或 base 不是有效的网址，则将抛出 TypeError。注意，会将给定的值强制转换为字符串。例如：

const myURL = new URL({ toString: () => 'https://example.org/' });
// https://example.org/ 拷贝
input 的主机名中出现的 Unicode 字符将使用 Punycode 算法自动转换为 ASCII。

const myURL = new URL('https://測試');
// https://xn--g6w251d/ 拷贝
如果事先不知道 input 是否是绝对的网址并且提供了 base，则建议验证 URL 对象的 origin 是否符合预期。

let myURL = new URL('http://Example.com/', 'https://example.org/');
// http://example.com/

myURL = new URL('https://Example.com/', 'https://example.org/');
// https://example.com/

myURL = new URL('foo://Example.com/', 'https://example.org/');
// foo://Example.com/

myURL = new URL('http:Example.com/', 'https://example.org/');
// http://example.com/

myURL = new URL('https:Example.com/', 'https://example.org/');
// https://example.org/Example.com/

myURL = new URL('foo:Example.com/', 'https://example.org/');
// foo:Example.com/ 拷贝

url.hash#
  < string >
  获取和设置网址的片段部分。

const myURL = new URL('https://example.org/foo#bar');
console.log(myURL.hash);
// Prints #bar

myURL.hash = 'baz';
console.log(myURL.href);
// Prints https://example.org/foo#baz 拷贝
分配给 hash 属性的值中包含的无效 URL 字符是 percent - encoded。选择要进行百分比编码的字符可能与 url.parse() 和 url.format() 方法产生的结果有所不同。


url.host#
  < string >
  获取和设置网址的主机部分。

const myURL = new URL('https://example.org:81/foo');
console.log(myURL.host);
// Prints example.org:81

myURL.host = 'example.com:82';
console.log(myURL.href);
// Prints https://example.com:82/foo 拷贝
分配给 host 属性的无效主机值将被忽略。


url.hostname#
  < string >
  获取和设置网址的主机名部分。url.host 和 url.hostname 之间的主要区别是 url.hostname 不包括端口。

const myURL = new URL('https://example.org:81/foo');
console.log(myURL.hostname);
// Prints example.org

// Setting the hostname does not change the port
myURL.hostname = 'example.com';
console.log(myURL.href);
// Prints https://example.com:81/foo

// Use myURL.host to change the hostname and port
myURL.host = 'example.org:82';
console.log(myURL.href);
// Prints https://example.org:82/foo 拷贝
分配给 hostname 属性的无效主机名值将被忽略。


url.href#
  < string >
  获取和设置序列化的网址。

const myURL = new URL('https://example.org/foo');
console.log(myURL.href);
// Prints https://example.org/foo

myURL.href = 'https://example.com/bar';
console.log(myURL.href);
// Prints https://example.com/bar 拷贝
获取 href 属性的值相当于调用 url.toString()。

将此属性的值设置为新值相当于使用 new URL(value) 创建新的 URL 对象。URL 对象的每个属性都将被修改。

如果分配给 href 属性的值不是有效的网址，则将抛出 TypeError。


url.origin#
版本历史
  < string >
  获取网址的源的只读的序列化。

const myURL = new URL('https://example.org/foo/bar?baz');
console.log(myURL.origin);
// Prints https://example.org 拷贝
const idnURL = new URL('https://測試');
console.log(idnURL.origin);
// Prints https://xn--g6w251d

console.log(idnURL.hostname);
// Prints xn--g6w251d 拷贝

url.password#
  < string >
  获取和设置网址的密码部分。

const myURL = new URL('https://abc:xyz@example.com');
console.log(myURL.password);
// Prints xyz

myURL.password = '123';
console.log(myURL.href);
// Prints https://abc:123@example.com/ 拷贝
分配给 password 属性的值中包含的无效 URL 字符是 percent - encoded。选择要进行百分比编码的字符可能与 url.parse() 和 url.format() 方法产生的结果有所不同。


url.pathname#
  < string >
  获取和设置网址的路径部分。

const myURL = new URL('https://example.org/abc/xyz?123');
console.log(myURL.pathname);
// Prints /abc/xyz

myURL.pathname = '/abcdef';
console.log(myURL.href);
// Prints https://example.org/abcdef?123 拷贝
分配给 pathname 属性的值中包含的无效 URL 字符是 percent - encoded。选择要进行百分比编码的字符可能与 url.parse() 和 url.format() 方法产生的结果有所不同。


url.port#
版本历史
  < string >
  获取和设置网址的端口部分。

端口值可以是数字，也可以是包含 0 到 65535（含）范围内的数字的字符串。将值设置为给定 protocol 的 URL 对象的默认端口将导致 port 值成为空字符串('')。

端口值可以是空字符串，在这种情况下端口取决于协议 / 方案：

协议	端口
"ftp"	21
"file"
"http"	80
"https"	443
"ws"	80
"wss"	443
为端口分配值后，该值将首先使用.toString() 转换为字符串。

如果该字符串无效但以数字开头，则将前导数字分配给 port。如果数字在上述范围之外，则将其忽略。

const myURL = new URL('https://example.org:8888');
console.log(myURL.port);
// Prints 8888

// Default ports are automatically transformed to the empty string
// (HTTPS protocol's default port is 443)
myURL.port = '443';
console.log(myURL.port);
// Prints the empty string
console.log(myURL.href);
// Prints https://example.org/

myURL.port = 1234;
console.log(myURL.port);
// Prints 1234
console.log(myURL.href);
// Prints https://example.org:1234/

// Completely invalid port strings are ignored
myURL.port = 'abcd';
console.log(myURL.port);
// Prints 1234

// Leading numbers are treated as a port number
myURL.port = '5678abcd';
console.log(myURL.port);
// Prints 5678

// Non-integers are truncated
myURL.port = 1234.5678;
console.log(myURL.port);
// Prints 1234

// Out-of-range numbers which are not represented in scientific notation
// will be ignored.
myURL.port = 1e10; // 10000000000, will be range-checked as described below
console.log(myURL.port);
// Prints 1234 拷贝
包含小数点的数字，例如浮点数或科学记数法中的数字，也不例外。小数点前的前导数字将被设置为网址的端口，假设它们是有效的：

myURL.port = 4.567e21;
console.log(myURL.port);
// Prints 4 (because it is the leading number in the string '4.567e21') 拷贝

url.protocol#
  < string >
  获取和设置网址的协议部分。

const myURL = new URL('https://example.org');
console.log(myURL.protocol);
// Prints https:

myURL.protocol = 'ftp';
console.log(myURL.href);
// Prints ftp://example.org/ 拷贝
分配给 protocol 属性的无效的网址协议值将被忽略。


特别计划#
版本历史
WHATWG URL 标准 认为少数 URL 协议方案在解析和序列化方面是特殊的。当使用这些特殊协议之一解析网址时，url.protocol 属性可能会更改为另一种特殊协议，但不能更改为非特殊协议，反之亦然。

例如，从 http 更改为 https 有效：

const u = new URL('http://example.org');
u.protocol = 'https';
console.log(u.href);
// https://example.org/ 拷贝
但是，从 http 更改为假设的 fish 协议并不是因为新协议并不特殊。

const u = new URL('http://example.org');
u.protocol = 'fish';
console.log(u.href);
// http://example.org/ 拷贝
同样，也不允许从非特殊协议更改为特殊协议：

const u = new URL('fish://example.org');
u.protocol = 'http';
console.log(u.href);
// fish://example.org 拷贝
根据 WHATWG 网址标准，特殊协议方案有 ftp、file、http、https、ws 和 wss。


url.search#
  < string >
  获取和设置网址的序列化的查询部分。

const myURL = new URL('https://example.org/abc?123');
console.log(myURL.search);
// Prints ?123

myURL.search = 'abc=xyz';
console.log(myURL.href);
// Prints https://example.org/abc?abc=xyz 拷贝
分配给 search 属性的值中出现的任何无效 URL 字符都将为 percent - encoded。选择要进行百分比编码的字符可能与 url.parse() 和 url.format() 方法产生的结果有所不同。


url.searchParams#
  < URLSearchParams >
  获取表示网址查询参数的 URLSearchParams 对象。该属性是只读的，但它提供的 URLSearchParams 对象可用于改变 URL 实例；要替换 URL 的全部查询参数，请使用 url.search 设置器。有关详细信息，请参阅 URLSearchParams 文档。

当使用.searchParams 修改 URL 时要小心，因为根据 WHATWG 规范，URLSearchParams 对象使用不同的规则来确定要对哪些字符进行百分比编码。例如，URL 对象不会对 ASCII 波浪号(~) 字符进行百分比编码，而 URLSearchParams 将始终对其进行编码：

const myURL = new URL('https://example.org/abc?foo=~bar');

console.log(myURL.search);  // prints ?foo=~bar

// Modify the URL via searchParams...
myURL.searchParams.sort();

console.log(myURL.search);  // prints ?foo=%7Ebar 拷贝

url.username#
  < string >
  获取和设置网址的用户名部分。

const myURL = new URL('https://abc:xyz@example.com');
console.log(myURL.username);
// Prints abc

myURL.username = '123';
console.log(myURL.href);
// Prints https://123:xyz@example.com/ 拷贝
分配给 username 属性的值中出现的任何无效 URL 字符都将为 percent - encoded。选择要进行百分比编码的字符可能与 url.parse() 和 url.format() 方法产生的结果有所不同。


url.toString()#
返回：<string>

  URL 对象上的 toString() 方法返回序列化的网址。返回值等同于 url.href 和 url.toJSON() 的值。


  url.toJSON()#
  返回：<string>

    URL 对象上的 toJSON() 方法返回序列化的网址。返回值等同于 url.href 和 url.toString() 的值。

    当 URL 对象用 JSON.stringify() 序列化时，会自动调用此方法。

    ¥This method is automatically called when an URL object is serialized with JSON.stringify().

    const myURLs = [
    new URL('https://www.example.com'),
    new URL('https://test.example.org'),
    ];
    console.log(JSON.stringify(myURLs));
    // Prints ["https://www.example.com/","https://test.example.org/"] 拷贝

    URL.createObjectURL(blob)#
    新增于: v16.7.0
    稳定性: 1 - 实验性的
    blob <Blob>

      返回：<string>

        创建表示给定的 <Blob> 对象并且可用于稍后检索 Blob 的 'blob:nodedata:...' 网址字符串。

          const {
            Blob,
            resolveObjectURL,
} = require('node:buffer');

          const blob = new Blob(['hello']);
          const id = URL.createObjectURL(blob);

          // later...

          const otherBlob = resolveObjectURL(id);
          console.log(otherBlob.size); 拷贝
          已注册的 <Blob> 存储的数据将保留在内存中，直到调用 URL.revokeObjectURL() 将其删除。

            Blob 对象已在当前线程中注册。如果使用工作线程，则在工作线程内注册的 Blob 对象将不能被其他工作线程或主线程使用。


            URL.revokeObjectURL(id)#
            新增于: v16.7.0
            稳定性: 1 - 实验性的
            id <string> 先前调用 URL.createObjectURL() 返回的 'blob:nodedata:... 网址字符串。

              删除由给定标识符标识的已存储的 <Blob>。尝试撤销未注册的 ID 将静默失败。


                URL.canParse(input[, base])#
                新增于: v19.9.0
                input <string> 要解析的绝对或相对的输入网址。如果 input 是相对的，则需要 base。如果 input 是绝对的，则忽略 base。如果 input 不是字符串，则首先是 转换为字符串。

                  base <string> 如果 input 不是绝对的，则为要解析的基本网址。如果 base 不是字符串，则首先是 转换为字符串。

                    返回：<boolean>

                      检查相对于 base 的 input 是否可以解析为 URL。

                      const isValid = URL.canParse('/foo', 'https://example.org/'); // true

                      const isNotValid = URL.canParse('/foo'); // false 拷贝

                      类：URLSearchParams#
                      版本历史
                      URLSearchParams API 提供对 URL 查询的读写访问。URLSearchParams 类也可以与以下四个构造函数之一单独使用。URLSearchParams 类也在全局对象上可用。

                      WHATWG URLSearchParams 接口和 querystring 模块具有相似的用途，但 querystring 模块的用途更通用，因为它允许自定义的分隔符（& 和 =）。换句话说，此 API 纯粹是为网址查询字符串而设计。

                      const myURL = new URL('https://example.org/?abc=123');
                      console.log(myURL.searchParams.get('abc'));
                      // Prints 123

                      myURL.searchParams.append('abc', 'xyz');
                      console.log(myURL.href);
                      // Prints https://example.org/?abc=123&abc=xyz

                      myURL.searchParams.delete('abc');
                      myURL.searchParams.set('a', 'b');
                      console.log(myURL.href);
                      // Prints https://example.org/?a=b

                      const newSearchParams = new URLSearchParams(myURL.searchParams);
                      // The above is equivalent to
                      // const newSearchParams = new URLSearchParams(myURL.search);

                      newSearchParams.append('a', 'c');
                      console.log(myURL.href);
                      // Prints https://example.org/?a=b
                      console.log(newSearchParams.toString());
                      // Prints a=b&a=c

                      // newSearchParams.toString() is implicitly called
                      myURL.search = newSearchParams;
                      console.log(myURL.href);
                      // Prints https://example.org/?a=b&a=c
                      newSearchParams.delete('a');
                      console.log(myURL.href);
                      // Prints https://example.org/?a=b&a=c 拷贝

                      new URLSearchParams()#
                      实例化新的空 URLSearchParams 对象。


                      new URLSearchParams(string)#
                      string <string> 查询字符串

                        将 string 解析为查询字符串，并使用它来实例化新的 URLSearchParams 对象。前导 '?'（如果存在）将被忽略。

                        let params;

                        params = new URLSearchParams('user=abc&query=xyz');
                        console.log(params.get('user'));
                        // Prints 'abc'
                        console.log(params.toString());
                        // Prints 'user=abc&query=xyz'

                        params = new URLSearchParams('?user=abc&query=xyz');
                        console.log(params.toString());
                        // Prints 'user=abc&query=xyz' 拷贝

                        new URLSearchParams(obj)#
                        新增于: v7.10.0, v6.13.0
                        obj <Object> 表示键值对集合的对象

                          使用查询哈希映射实例化新的 URLSearchParams 对象。obj 的每个属性的键和值总是被强制转换为字符串。

                          与 querystring 模块不同，不允许以数组值的形式出现重复的键。数组使用 array.toString() 字符串化，它简单地用逗号连接所有数组元素。

                          const params = new URLSearchParams({
                            user: 'abc',
                          query: ['first', 'second'],
});
                          console.log(params.getAll('query'));
                          // Prints [ 'first,second' ]
                          console.log(params.toString());
                          // Prints 'user=abc&query=first%2Csecond' 拷贝

                          new URLSearchParams(iterable)#
                          新增于: v7.10.0, v6.13.0
                          iterable <Iterable> 元素为键值对的可迭代对象

                            以类似于 Map 的构造函数的方式使用可迭代映射实例化新的 URLSearchParams 对象。iterable 可以是 Array 或任何可迭代对象。这意味着 iterable 可以是另一个 URLSearchParams，在这种情况下，构造函数将简单地创建提供的 URLSearchParams 的克隆。iterable 的元素是键值对，并且本身可以是任何可迭代对象。

                            允许重复的键。

                            let params;

                            // Using an array
                            params = new URLSearchParams([
                            ['user', 'abc'],
                            ['query', 'first'],
                            ['query', 'second'],
                            ]);
                            console.log(params.toString());
                            // Prints 'user=abc&query=first&query=second'

                            // Using a Map object
                            const map = new Map();
                            map.set('user', 'abc');
                            map.set('query', 'xyz');
                            params = new URLSearchParams(map);
                            console.log(params.toString());
                            // Prints 'user=abc&query=xyz'

                            // Using a generator function
                            function* getQueryPairs() {
                              yield['user', 'abc'];
                            yield ['query', 'first'];
                            yield ['query', 'second'];
}
                            params = new URLSearchParams(getQueryPairs());
                            console.log(params.toString());
                            // Prints 'user=abc&query=first&query=second'

                            // Each key-value pair must have exactly two elements
                            new URLSearchParams([
                            ['user', 'abc', 'error'],
                            ]);
                            // Throws TypeError [ERR_INVALID_TUPLE]:
                            //        Each query pair must be an iterable [name, value] tuple 拷贝

                            urlSearchParams.append(name, value)#
                            name <string>

                              value <string>

                                将新的名称-值对追加到查询字符串。


                                urlSearchParams.delete(name[, value])#
                                版本历史
                                name <string>

                                  value <string>

                                    如果提供了 value，则删除名称为 name 且值为 value 的所有名称-值对。

                                    如果未提供 value，则删除名称为 name 的所有名称-值对。


                                    urlSearchParams.entries()#
                                    返回：<Iterator>

                                      在查询中的每个名称-值对上返回 ES6 Iterator。迭代器的每一项都是 JavaScript Array。Array 的第一项是 name，Array 的第二项是 value。

                                      urlSearchParams[@@iterator]() 的别名。


                                      urlSearchParams.forEach(fn[, thisArg])#
                                      版本历史
                                      fn <Function> 为查询中的每个名称-值对调用

                                        thisArg <Object> 在调用 fn 时用作 this 值

                                          迭代查询中的每个名称-值对并调用给定的函数。

                                          const myURL = new URL('https://example.org/?a=b&c=d');
myURL.searchParams.forEach((value, name, searchParams) => {
                                            console.log(name, value, myURL.searchParams === searchParams);
});
                                          // Prints:
                                          //   a b true
                                          //   c d true 拷贝

                                          urlSearchParams.get(name)#
                                          name <string>

                                            返回：<string> | <null> 如果没有具有给定 name 的名称/值对，则为字符串或 null。

                                              返回名称为 name 的第一个名称-值对的值。如果没有这样的对，则返回 null。


                                              urlSearchParams.getAll(name)#
                                              name <string>

                                                返回：<string[]>

                                                返回名称为 name 的所有名称-值对的值。如果没有这样的对，则返回空数组。


                                                urlSearchParams.has(name[, value])#
                                                版本历史
                                                name <string>

                                                  value <string>

                                                    返回：<boolean>

                                                      检查 URLSearchParams 对象是否包含基于 name 和可选的 value 参数的键值对。

                                                      如果提供了 value，则当存在具有相同 name 和 value 的名称-值对时返回 true。

                                                      如果未提供 value，则如果至少有一个名称为 name 的名称-值对，则返回 true。


                                                      urlSearchParams.keys()#
                                                      返回：<Iterator>

                                                        在每个名称-值对的名称上返回 ES6 Iterator。

                                                        const params = new URLSearchParams('foo=bar&foo=baz');
                                                        for (const name of params.keys()) {
                                                          console.log(name);
}
                                                        // Prints:
                                                        //   foo
                                                        //   foo 拷贝

                                                        urlSearchParams.set(name, value)#
                                                        name <string>

                                                          value <string>

                                                            将与 name 关联的 URLSearchParams 对象中的值设置为 value。如果存在任何名称为 name 的预先存在的名称-值对，则将第一个此类对的值设置为 value 并删除所有其他名称。如果没有，则将名称-值对追加到查询字符串。

                                                            const params = new URLSearchParams();
                                                            params.append('foo', 'bar');
                                                            params.append('foo', 'baz');
                                                            params.append('abc', 'def');
                                                            console.log(params.toString());
                                                            // Prints foo=bar&foo=baz&abc=def

                                                            params.set('foo', 'def');
                                                            params.set('xyz', 'opq');
                                                            console.log(params.toString());
                                                            // Prints foo=def&abc=def&xyz=opq 拷贝

                                                            urlSearchParams.size#
                                                            新增于: v19.8.0
                                                            参数条目的总数。


                                                            urlSearchParams.sort()#
                                                            新增于: v7.7.0, v6.13.0
                                                            按名称对所有现有的名称-值对进行就地排序。排序是使用 稳定排序算法 完成的，因此保留了具有相同名称的名称-值对之间的相对顺序。

                                                            该方法尤其可用于增加缓存命中。

                                                            const params = new URLSearchParams('query[]=abc&type=search&query[]=123');
                                                            params.sort();
                                                            console.log(params.toString());
                                                            // Prints query%5B%5D=abc&query%5B%5D=123&type=search 拷贝

                                                            urlSearchParams.toString()#
                                                            返回：<string>

                                                              返回序列化为字符串的搜索参数，必要时使用百分比编码的字符。


                                                              urlSearchParams.values()#
                                                              返回：<Iterator>

                                                                在每个名称-值对的值上返回 ES6 Iterator。


                                                                urlSearchParams[Symbol.iterator]()#
                                                                返回：<Iterator>

                                                                  在查询字符串中的每个名称-值对上返回 ES6 Iterator。迭代器的每一项都是 JavaScript Array。Array 的第一项是 name，Array 的第二项是 value。

                                                                  urlSearchParams.entries() 的别名。

                                                                  const params = new URLSearchParams('foo=bar&xyz=baz');
                                                                  for (const [name, value] of params) {
                                                                    console.log(name, value);
}
                                                                  // Prints:
                                                                  //   foo bar
                                                                  //   xyz baz 拷贝

                                                                  url.domainToASCII(domain)#
                                                                  版本历史
                                                                  domain <string>

                                                                    返回：<string>

                                                                      返回 domain 的 Punycode ASCII 序列化。如果 domain 是无效域，则返回空字符串。

                                                                      它执行与 url.domainToUnicode() 相反的操作。

                                                                      const url = require('node:url');

                                                                      console.log(url.domainToASCII('español.com'));
                                                                      // Prints xn--espaol-zwa.com
                                                                      console.log(url.domainToASCII('中文.com'));
                                                                      // Prints xn--fiq228c.com
                                                                      console.log(url.domainToASCII('xn--iñvalid.com'));
                                                                      // Prints an empty string拷贝

                                                                      url.domainToUnicode(domain)#
                                                                      版本历史
                                                                      domain <string>

                                                                        返回：<string>

                                                                          返回 domain 的 Unicode 序列化。如果 domain 是无效域，则返回空字符串。

                                                                          它执行与 url.domainToASCII() 相反的操作。

                                                                          const url = require('node:url');

                                                                          console.log(url.domainToUnicode('xn--espaol-zwa.com'));
                                                                          // Prints español.com
                                                                          console.log(url.domainToUnicode('xn--fiq228c.com'));
                                                                          // Prints 中文.com
                                                                          console.log(url.domainToUnicode('xn--iñvalid.com'));
                                                                          // Prints an empty string拷贝

                                                                          url.fileURLToPath(url)#
                                                                          新增于: v10.12.0
                                                                          url <URL> | <string> 要转换为路径的文件网址字符串或网址对象。

                                                                            返回：<string> 完全解析的特定于平台的 Node.js 文件路径。

                                                                              此函数可确保正确解码百分比编码字符，并确保跨平台有效的绝对路径字符串。

                                                                              const {fileURLToPath} = require('node:url');
                                                                              new URL('file:///C:/path/').pathname;      // Incorrect: /C:/path/
                                                                              fileURLToPath('file:///C:/path/');         // Correct:   C:\path\ (Windows)

                                                                              new URL('file://nas/foo.txt').pathname;    // Incorrect: /foo.txt
                                                                              fileURLToPath('file://nas/foo.txt');       // Correct:   \\nas\foo.txt (Windows)

                                                                              new URL('file:///你好.txt').pathname;      // Incorrect: /%E4%BD%A0%E5%A5%BD.txt
                                                                              fileURLToPath('file:///你好.txt');         // Correct:   /你好.txt (POSIX)

                                                                              new URL('file:///hello world').pathname;   // Incorrect: /hello%20world
                                                                              fileURLToPath('file:///hello world');      // Correct:   /hello world (POSIX)拷贝

                                                                              url.format(URL[, options])#
                                                                              新增于: v7.6.0
                                                                              URL <URL> 一个 WHATWG URL 对象

                                                                                options <Object>

                                                                                  auth <boolean> 如果序列化 URL 字符串应包含用户名和密码，则为 true，否则为 false。默认值：true。

                                                                                    fragment <boolean> 如果序列化 URL 字符串应包含片段，则为 true，否则为 false。默认值：true。

                                                                                      search <boolean> 如果序列化 URL 字符串应包含搜索查询，则为 true，否则为 false。默认值：true。

                                                                                        unicode <boolean> true 如果 URL 字符串的主机组件中出现的 Unicode 字符应直接编码，而不是 Punycode 编码。默认值：false。

                                                                                          返回：<string>

                                                                                            返回 WHATWG URL 对象的 URL String 表示的可自定义序列化。

                                                                                            网址对象具有 toString() 方法和 href 属性，用于返回网址的字符串序列化。但是，这些都不能以任何方式自定义。url.format(URL[, options]) 方法允许对输出进行基本的自定义。

                                                                                            const url = require('node:url');
                                                                                            const myURL = new URL('https://a:b@測試?abc#foo');

                                                                                            console.log(myURL.href);
                                                                                            // Prints https://a:b@xn--g6w251d/?abc#foo

                                                                                            console.log(myURL.toString());
                                                                                            // Prints https://a:b@xn--g6w251d/?abc#foo

                                                                                            console.log(url.format(myURL, {fragment: false, unicode: true, auth: false }));
                                                                                            // Prints 'https://測試/?abc'拷贝

                                                                                            url.pathToFileURL(path)#
                                                                                            新增于: v10.12.0
                                                                                            path <string> 要转换为文件网址的路径。

                                                                                              返回：<URL> 文件网址对象。

                                                                                                该函数确保 path 被绝对解析，并且在转换为文件网址时正确编码网址控制字符。

                                                                                                const {pathToFileURL} = require('node:url');
                                                                                                new URL(__filename);                  // Incorrect: throws (POSIX)
                                                                                                new URL(__filename);                  // Incorrect: C:\... (Windows)
                                                                                                pathToFileURL(__filename);            // Correct:   file:///... (POSIX)
                                                                                                pathToFileURL(__filename);            // Correct:   file:///C:/... (Windows)

                                                                                                new URL('/foo#1', 'file:');           // Incorrect: file:///foo#1
                                                                                                pathToFileURL('/foo#1');              // Correct:   file:///foo%231 (POSIX)

                                                                                                new URL('/some/path%.c', 'file:');    // Incorrect: file:///some/path%.c
                                                                                                pathToFileURL('/some/path%.c');       // Correct:   file:///some/path%25.c (POSIX)拷贝

                                                                                                url.urlToHttpOptions(url)#
                                                                                                版本历史
                                                                                                url <URL> 要转换为选项对象的 WHATWG URL 对象。

                                                                                                  返回：<Object> 选项对象

                                                                                                    protocol <string> 要使用的协议。

                                                                                                      hostname <string> 要向其触发请求的服务器的域名或 IP 地址。

                                                                                                        hash <string> 网址的片段部分。

                                                                                                          search <string> 网址的序列化的查询部分。

                                                                                                            pathname <string> 网址的路径部分。

                                                                                                              path <string> 请求的路径。应包括查询字符串（如果有）。E.G.'/index.html?page=12'。当请求路径包含非法字符时抛出异常。目前，只有空格被拒绝，但将来可能会改变。

                                                                                                                href <string> 序列化的网址。

                                                                                                                  port <number> 远程服务器的端口。

                                                                                                                    auth <string> 基本身份验证，即 'user:password' 计算授权标头。

                                                                                                                      该实用函数按照 http.request() 和 https.request() API 的预期将网址对象转换为普通选项对象。

                                                                                                                      const {urlToHttpOptions} = require('node:url');
                                                                                                                      const myURL = new URL('https://a:b@測試?abc#foo');

                                                                                                                      console.log(urlToHttpOptions(myURL));
/*
{
                                                                                                                        protocol: 'https:',
                                                                                                                      hostname: 'xn--g6w251d',
                                                                                                                      hash: '#foo',
                                                                                                                      search: '?abc',
                                                                                                                      pathname: '/',
                                                                                                                      path: '/?abc',
                                                                                                                      href: 'https://a:b@xn--g6w251d/?abc#foo',
                                                                                                                      auth: 'a:b'
}
                                                                                                                      */拷贝

                                                                                                                      旧版 URL API#
                                                                                                                      版本历史
                                                                                                                      稳定性: 3 - 旧版：请改用 WHATWG URL API。

                                                                                                                      旧版 urlObject#
                                                                                                                      版本历史
                                                                                                                      稳定性: 3 - 旧版：请改用 WHATWG URL API。
                                                                                                                      旧版的 urlObject（require('node:url').Url 或 import {Url} from 'node:url'）由 url.parse() 函数创建和返回。


                                                                                                                      urlObject.auth#
                                                                                                                      auth 属性是 URL 的用户名和密码部分，也称为用户信息。此字符串子集跟在 protocol 和双斜杠（如果存在）之后，并在 host 组件之前，由 @ 分隔。该字符串要么是用户名，要么是由 : 分隔的用户名和密码。

                                                                                                                      例如：'user:pass'。


                                                                                                                      urlObject.hash#
                                                                                                                      hash 属性是网址的片段标识符部分，包括前导 # 字符。

                                                                                                                      例如：'#hash'。


                                                                                                                      urlObject.host#
                                                                                                                      host 属性是网址的完整小写主机部分，包括 port（如果指定）。

                                                                                                                      例如：'sub.example.com:8080'。


                                                                                                                      urlObject.hostname#
                                                                                                                      hostname 属性是 host 组件的小写主机名部分，不包括 port。

                                                                                                                      例如：'sub.example.com'。


                                                                                                                      urlObject.href#
                                                                                                                      href 属性是将 protocol 和 host 组件都转换为小写的完整网址字符串。

                                                                                                                      例如：'http://user:pass@sub.example.com:8080/p/a/t/h?query=string#hash'。


                                                                                                                      urlObject.path#
                                                                                                                      path 属性是 pathname 和 search 组件的串联。

                                                                                                                      例如：'/p/a/t/h?query=string'。

                                                                                                                      不执行 path 的解码。


                                                                                                                      urlObject.pathname#
                                                                                                                      pathname 属性包含网址的整个路径部分。这是 host（包括 port）之后和 query 或 hash 组件开始之前的所有内容，由 ASCII 问号 (?) 或哈希 (#) 字符分隔。

                                                                                                                      例如：'/p/a/t/h'。

                                                                                                                      不执行路径字符串的解码。


                                                                                                                      urlObject.port#
                                                                                                                      port 属性是 host 组件的数字端口部分。

                                                                                                                      例如：'8080'。


                                                                                                                      urlObject.protocol#
                                                                                                                      protocol 属性标识网址的小写协议方案。

                                                                                                                      例如：'http:'。


                                                                                                                      urlObject.query#
                                                                                                                      query 属性要么是不带前导 ASCII 问号 (?) 的查询字符串，要么是 querystring 模块的 parse() 方法返回的对象。query 属性是字符串还是对象由传给 url.parse() 的 parseQueryString 参数决定。

                                                                                                                      例如：'query=string' 或 {'query': 'string'}。

                                                                                                                      如果作为字符串返回，则不执行查询字符串的解码。如果作为对象返回，则键和值都会被解码。


                                                                                                                      urlObject.search#
                                                                                                                      search 属性由 URL 的整个 "请求参数" 部分组成，包括前导 ASCII 问号 (?) 字符。

                                                                                                                      例如：'?query=string'。

                                                                                                                      不执行查询字符串的解码。


                                                                                                                      urlObject.slashes#
                                                                                                                      如果 protocol 中的冒号后需要两个 ASCII 正斜杠字符 (/)，则 slashes 属性是值为 true 的 boolean。


                                                                                                                      url.format(urlObject)#
                                                                                                                      版本历史
                                                                                                                      稳定性: 3 - 旧版：请改用 WHATWG URL API。
                                                                                                                      urlObject <Object> | <string> 网址对象（由 url.parse() 返回或以其他方式构造）。如果是字符串，则通过将其传给 url.parse() 将其转换为对象。

                                                                                                                        url.format() 方法返回从 urlObject 派生的格式化网址字符串。

                                                                                                                        const url = require('node:url');
                                                                                                                        url.format({
                                                                                                                          protocol: 'https',
                                                                                                                        hostname: 'example.com',
                                                                                                                        pathname: '/some/path',
                                                                                                                        query: {
                                                                                                                          page: 1,
                                                                                                                        format: 'json',
  },
});

                                                                                                                        // => 'https://example.com/some/path?page=1&format=json' 拷贝
                                                                                                                        如果 urlObject 不是对象或字符串，则 url.format() 将抛出 TypeError。

                                                                                                                        格式化过程如下：

                                                                                                                        创建新的空字符串 result。

                                                                                                                        如果 urlObject.protocol 是字符串，则按原样附加到 result。

                                                                                                                        否则，如果 urlObject.protocol 不是 undefined 并且不是字符串，则抛出 Error。

                                                                                                                        对于不以 ASCII 冒号 (:) 字符结尾的 urlObject.protocol 的所有字符串值，字面字符串 : 将附加到 result。

                                                                                                                        如果以下任一条件为真，则字面字符串 // 将附加到 result：

                                                                                                                        urlObject.slashes 属性为真；

                                                                                                                        urlObject.protocol 以 http、https、ftp、gopher 或 file 开头；

                                                                                                                        如果 urlObject.auth 属性的值为真，并且 urlObject.host 或 urlObject.hostname 不是 undefined，则 urlObject.auth 的值将被强制转换为字符串并附加到 result 后跟字面串 @。

                                                                                                                        如果 urlObject.host 属性为 undefined，则：

                                                                                                                        如果 urlObject.hostname 是字符串，则将其附加到 result。

                                                                                                                        否则，如果 urlObject.hostname 不是 undefined 并且不是字符串，则抛出 Error。

                                                                                                                        如果 urlObject.port 属性值为真，并且 urlObject.hostname 不是 undefined：

                                                                                                                        字面量字符串 : 附加到 result，并且

                                                                                                                        urlObject.port 的值被强制转换为字符串并附加到 result。

                                                                                                                        否则，如果 urlObject.host 属性值为真，则将 urlObject.host 的值强制转换为字符串并附加到 result。

                                                                                                                        如果 urlObject.pathname 属性是非空的字符串：

                                                                                                                        如果 urlObject.pathname 不以 ASCII 正斜杠 (/) 开头，则将字面字符串 '/' 附加到 result。

                                                                                                                        urlObject.pathname 的值附加到 result。

                                                                                                                        否则，如果 urlObject.pathname 不是 undefined 并且不是字符串，则抛出 Error。

                                                                                                                        如果 urlObject.search 属性是 undefined 并且如果 urlObject.query 属性是 Object，则字面串 ? 附加到 result，然后是调用 querystring 模块的 stringify() 方法的输出，并传入 urlObject.query 的值。

                                                                                                                        否则，如果 urlObject.search 是一个字符串：

                                                                                                                        如果 urlObject.search 的值不以 ASCII 问号 (?) 字符开头，则将字面字符串 ? 附加到 result。

                                                                                                                        urlObject.search 的值附加到 result。

                                                                                                                        否则，如果 urlObject.search 不是 undefined 并且不是字符串，则抛出 Error。

                                                                                                                        如果 urlObject.hash 属性是字符串：

                                                                                                                        如果 urlObject.hash 的值不以 ASCII 散列 (#) 字符开头，则将字面字符串 # 附加到 result。

                                                                                                                        urlObject.hash 的值附加到 result。

                                                                                                                        否则，如果 urlObject.hash 属性不是 undefined 并且不是字符串，则抛出 Error。

                                                                                                                        result 返回。


                                                                                                                        url.parse(urlString[, parseQueryString[, slashesDenoteHost]])#
                                                                                                                        版本历史
                                                                                                                        稳定性: 0 - 已弃用：请改用 WHATWG URL API。
                                                                                                                        urlString <string> 要解析的 URL 字符串。

                                                                                                                          parseQueryString <boolean> 如果为 true，则 query 属性将始终设置为 querystring 模块的 parse() 方法返回的对象。如果为 false，则返回的网址对象上的 query 属性将是未解析、未解码的字符串。默认值：false。

                                                                                                                            slashesDenoteHost <boolean> 如果为 true，则字面串 // 之后和下一个 / 之前的第一个令牌将被解释为 host。例如，给定 //foo/bar，结果将是 {host: 'foo', pathname: '/bar'} 而不是 {pathname: '//foo/bar'}。默认值：false。

                                                                                                                              url.parse() 方法接受网址字符串，解析并返回网址对象。

                                                                                                                              如果 urlString 不是字符串，则抛出 TypeError。

                                                                                                                              如果 auth 属性存在但无法解码，则抛出 URIError。

                                                                                                                              url.parse() 使用一种宽松的、非标准的算法来解析 URL 字符串。它很容易出现安全问题，例如 主机名欺骗 和用户名和密码的不正确处理。不要使用不受信任的输入。不针对 url.parse() 漏洞发布 CVE。改用 WHATWG URL API。


                                                                                                                              url.resolve(from, to)#
                                                                                                                              版本历史
                                                                                                                              稳定性: 3 - 旧版：请改用 WHATWG URL API。
                                                                                                                              from <string> 如果 to 是相对的 URL，则使用的基本的 URL。

                                                                                                                                to <string> 要解析的目标 URL。

                                                                                                                                  url.resolve() 方法以类似于 Web 浏览器解析锚标记的方式解析相对于基本 URL 的目标 URL。

                                                                                                                                  const url = require('node:url');
                                                                                                                                  url.resolve('/one/two/three', 'four');         // '/one/two/four'
                                                                                                                                  url.resolve('http://example.com/', '/one');    // 'http://example.com/one'
                                                                                                                                  url.resolve('http://example.com/one', '/two'); // 'http://example.com/two' 拷贝
                                                                                                                                  使用 WHATWG URL API 实现相同的结果：

                                                                                                                                  function resolve(from, to) {
  const resolvedUrl = new URL(to, new URL(from, 'resolve://'));
                                                                                                                                  if (resolvedUrl.protocol === 'resolve:') {
    // `from` is a relative URL.
    const {pathname, search, hash} = resolvedUrl;
                                                                                                                                  return pathname + search + hash;
  }
                                                                                                                                  return resolvedUrl.toString();
}

                                                                                                                                  resolve('/one/two/three', 'four');         // '/one/two/four'
                                                                                                                                  resolve('http://example.com/', '/one');    // 'http://example.com/one'
                                                                                                                                  resolve('http://example.com/one', '/two'); // 'http://example.com/two' 拷贝


                                                                                                                                  URL 中的百分比编码#
                                                                                                                                  网址只允许包含一定范围的字符。任何超出该范围的字符都必须进行编码。这些字符的编码方式以及要编码的字符完全取决于字符在网址结构中的位置。


                                                                                                                                  旧版 API#
                                                                                                                                  在旧版 API 中，空格 (' ') 和以下字符将在网址对象的属性中自动转义：

< > " ` \r \n \t { } | \ ^ ' 拷贝
                                                                                                                                    例如，ASCII 空格字符 (' ') 被编码为 %20。ASCII 正斜杠 (/) 字符编码为 %3C。


                                                                                                                                    WHATWG API#
                                                                                                                                    WHATWG URL 标准 使用比 Legacy API 使用的方法更具选择性和更细粒度的方法来选择编码字符。

                                                                                                                                    WHATWG 算法定义了四个 "百分比编码集"，它们描述了必须进行百分比编码的字符范围：

                                                                                                                                    C0 控制百分比编码集包括 U+0000 到 U+001F（含）范围内的代码点以及大于 U+007E (~) 的所有代码点。

                                                                                                                                    片段百分比编码集包括 C0 控制百分比编码集和代码点 U+0020 SPACE、U+0022 (")、U+003C (<)、U+003E (>) 和 U+0060 (`) 。

                                                                                                                                    路径百分比编码集包括 C0 控制百分比编码集和代码点 U+0020 SPACE、U+0022 (")、U+0023 (#)、U+003C (<)、U+003E (>)、 U+003F (?)、U+0060 (`)、U+007B ({), and U+007D (})。

                                                                                                                                    用户信息编码集包括路径百分比编码集和代码点 U+002F (/)、U+003A (:)、U+003B (;)、U+003D (=)、U+0040 (@)、U +005B ([) 至 U+005E(^) 和 U+007C (|)。

                                                                                                                                    userinfo 百分比编码集专门用于在 URL 中编码的用户名和密码。路径百分比编码集用于大多数 URL 的路径。片段百分比编码集用于 URL 片段。C0 控制百分比编码集用于某些特定条件下的主机和路径，以及所有其他情况。

                                                                                                                                    当主机名中出现非 ASCII 字符时，主机名将使用 Punycode 算法进行编码。但是请注意，主机名可能同时包含 Punycode 编码字符和百分比编码字符：

                                                                                                                                    const myURL = new URL('https://%CF%80.example.com/foo');
                                                                                                                                    console.log(myURL.href);
                                                                                                                                    // Prints https://xn--1xa.example.com/foo
                                                                                                                                    console.log(myURL.origin);
// Prints https://xn--1xa.example.com 拷贝