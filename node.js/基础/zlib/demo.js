const fs = require('fs')
const zlib = require('zlib')
const http = require('http')

const gzip = zlib.createGzip()

http.createServer((req, res) => {

  res.writeHead({
    'Content-Type': 'application/x-javasrcipt;charset=utf-8',
    'Content-Encoding': 'gzip'
  })

  fs.createReadStream('./index.js')
    .pipe(gzip)
    .pipe(res)

}).listen(3000, () => {
  console.log('server is running: 3000')
})


