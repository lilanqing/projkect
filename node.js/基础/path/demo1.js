// const path = require('path');
// __filename 表示当前正在执行的脚本的文件名 (包含目录)
// __dirname 表示当前执行脚本所在的目录。
// console.log('__filename', __filename);
// console.log('__dirname', __dirname);
// path.basename() 路径中的文件名 最后一个/之后
// path.extname() 文件名中的扩展名
// path.normalize() // 整理不整齐的路径成标准路径
// path.parse() //解析路径
// path.format() // 序列话路径
// path.isAbsolute() //判断是否是绝对路径
// path.join() //拼接路径
// path.resolve() //将路径解析为绝对路径  如果没有传入 path 片段，则 path.resolve() 将返回当前工作目录的绝对路径。
// process.cwd() // 将返回当前工作目录的绝对路径


// console.log(path.resolve([from],to));
// log(
//   path.resolve('index.js'),
// );
// import.meta.url 表示一个模块在浏览器和 Node.js 的绝对路径。该特性属于 es2020 的一部分
// log('import.meta', import.meta.url);


// console.log('__dirname', __dirname);
// console.log('__filename', __filename);
// console.log('process.cwd', process.cwd());
// console.log('path.join', path.join());
// console.log('==path.resolve()==', path.resolve(__dirname, 'test', 'demos'));
console.log('==path.resolve()==', path.resolve());





