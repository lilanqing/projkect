// 存储二进制的类数组
// const buf1 = Buffer.alloc(10)
// const buf2 = Buffer.alloc(10, 1)
// const buf3 = Buffer.from([1, 2, 3])
// const buf4 = Buffer.from('test')
// console.log('==buf1====>', buf1)
// console.log('==buf2====>', buf2)
// console.log('==buf3====>', buf3)
// console.log('==buf4====>', buf4)

// console.log('==export====>', exports)
// console.log('==module.export====>', module.exports)

// console.log('==exports===', module);

let str = 'Hello NodeJs'
const buf = Buffer.from(str)
console.log('==buf====>', buf)


// 计算机存的二进制数据,在显示的时候会以16进制的形式显示
// 只要是打印数字, 一定是10禁进制

