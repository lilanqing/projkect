import Router from 'koa-router';
import userRouter from 'user';
import listRouter from 'list';

const router = new Router();

router.prefix('/api');
router.use('/user', userRouter.routes(), router.allowedMethods());
router.use('/list', listRouter.routes(), router.allowedMethods());
router.redirect('/', '/user');

export default router;



