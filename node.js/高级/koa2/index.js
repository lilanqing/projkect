import path from 'node:path'
import Koa from 'koa'
import router from './routes'
import staticPublic from 'koa-static'
import bodyParser from 'koa-bodyparser'
import views from 'koa-views'
const app = new Koa()


app.use(staticPublic(path.join(views, 'public')))
app.use(views(path.join(__dirname, 'views'), { extension: 'ejs' }))
app.use(bodyParser())
app.use(router.routes()).use(router.allowedMethods())



app.use(async (ctx, next) => {
  // ctx.query
  // ctx.querystring
  // ctx.request.body
  ctx.set('token', 'aaa') //设置响应头
  ctx.response.body = 'Hello Koa.Js'
})

app.listen(3001, () => {
  console.log('server is running at http://localhost:3000')
})