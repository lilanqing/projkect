const { resolve } = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');

module.exports = {
  entry: './src/js/index.js',
  output: {
    filename: 'js/[name].js',
    path: resolve(__dirname, 'build'),
  },
  module: {
    rules: [
      {
        test: /\.css$/,
        use: ['style-loader', 'css-loader'],
      },
    ],
  },
  plugins: [new HtmlWebpackPlugin()],
  mode: 'development',
  resolve: {
    alias: {
      $css: resolve(__dirname, 'src/css'),
    },
    extensions: ['.js', '.json', '.jsx', '.css'],
    modules: [resolve(__dirname, '../../node_modules'), 'node_modules'],
  },
  devServer: {
    // 运行代码的目录
    // 配置 DevServer HTTP 服务器的文件根目录。 默认情况下为当前执行目录，通常是项目根目录
    contentBase: resolve(__dirname, 'build'),
    // 例如你想把项目根目录下的 public 目录设置成 DevServer 服务器的文件根目录，你可以这样配置：
    // contentBase: path.join(__dirname, 'public')

    // 监视 contentBase 目录下的所有文件，一旦文件变化就会 reload
    watchContentBase: true,
    watchOptions: {
      // 忽略文件
      ignored: /node_modules/,
    },
    // 启动gzip压缩
    compress: true,
    // 端口号
    port: 5000,
    // 域名 配置项用于配置 DevServer 服务监听的地址。
    host: 'localhost', // 127.0.0.1
    // 例如你想要局域网中的其它设备访问你本地的服务
    // host: '0.0.0.0',

    // DevServer 默认使用 HTTP 协议服务，它也能通过 HTTPS 协议服务。 有些情况下你必须使用 HTTPS
    // DevServer 会自动的为你生成一份 HTTPS 证书。
    https: true
    // 如果你想用自己的证书可以这样配置：
    // https: {
    //   key: fs.readFileSync('path/to/server.key'),
    //   cert: fs.readFileSync('path/to/server.crt'),
    //   ca: fs.readFileSync('path/to/ca.pem')
    // }

    // 自动打开浏览器
    open: true,
    // 开启HMR功能
    hot: true,
    hotOnly: true,
    // 不要显示启动服务器日志信息
    clientLogLevel: 'none',
    // 除了一些基本启动信息以外，其他内容都不要显示
    quiet: true,
    // 如果出错了，不要全屏提示~
    overlay: false,
    // 服务器代理 --> 解决开发环境跨域问题
    proxy: {
      // 一旦devServer(5000)服务器接受到 /api/xxx 的请求，就会把请求转发到另外一个服务器(3000)
      '/api': {
        // secure: false, // 如果是https代理，配置这项
        target: 'http://localhost:3000',
        changeOrigin: true,
        // 发送请求时，请求路径重写：将 /api/xxx --> /xxx （去掉/api）
        pathRewrite: {
          '^/api': '',
        },
      },
    },
  },
};
