const { resolve } = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const TerserWebpackPlugin = require('terser-webpack-plugin');

// import $ from 'jquery';
// import echarts from 'echarts';
// $('.my-element');

module.exports = {
  target:'web' // 针对浏览器 (默认)，所有代码都集中在一个文件里
  target:'node' // 针对 Node.js，使用 require 语句加载 Chunk 代码
  devtool: 'source-map',// 默认值是 false 即不生成 Source Map，
  // Externals 用来告诉 Webpack 要构建的代码中使用了哪些不用被打包的模块
  externals:{
    // import $ from 'jquery';
    // 把导入语句里的 jquery 替换成运行环境里的全局变量 jQuery
    jquery: 'jQuery'
    echarts:'echarts'
  }
};
