/*
 * @Author: your name
 * @Date: 2021-12-15 10:39:26
 * @LastEditTime: 2021-12-15 14:31:24
 * @LastEditors: your name
 * @Description: 打开koroFileHeader查看配置 进行设置: https://github.com/OBKoro1/koro1FileHeader/wiki/%E9%85%8D%E7%BD%AE
 * @FilePath: \vue3.0-cli-ts\webpack\5.webpack配置详解\30.output\webpack.config.js
 */
const { resolve } = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const { hash } = require('spark-md5');

// hash 代表一次编译操作的 Hash 值。
module.exports = {
  entry: './src/index.js',
  output: {
    // chunkhash
    // contenthash
    // hash
    // 文件名称（指定名称+目录）
    filename: 'js/[name].js',
    filename: 'js/[name].[chunkhash:10].[ext]',
    // 输出文件目录（将来所有资源输出的公共目录）
    path: resolve(__dirname, 'build'),
    // 所有资源引入公共路径前缀 --> 'imgs/a.jpg' --> '/imgs/a.jpg'
    publicPath: '/',
    //（默认是'' ， 表示开启服务器devsever后 index.html文件内部资源的引用路径  **域名 + publicPath + flilname** ）
    // 如果打包够想本地预览index.html设为'./'  默认设为'/'
    // filename:'[name]_[chunkhash:8].js'
    // publicPath: 'https://cdn.example.com/assets/'
    // <script src='https://cdn.example.com/assets/a_12345678.js'></script>

     // 为动态加载的 Chunk 配置输出文件的名称
    chunkFilename: 'js/[name]_chunk.js', // 非入口chunk的名称  chunkFilename 只用于指定在运行过程中生成的 Chunk 在输出时的文件名称
    chunkFilename:'js/[name].[contentchunk].js'
    
    // library: '[name]', // 配置导出库的名称。
    // 配置以何种方式导出库。
    // libraryTarget: 'window' // 变量名添加到哪个上 browser
    // libraryTarget: 'global' // 变量名添加到哪个上 node
    // libraryTarget: 'commonjs2'
    // / 可以是 umd | umd2 | commonjs2 | commonjs | amd | this | var | assign | window | global | jsonp ，
  },
  plugins: [new HtmlWebpackPlugin()],
  mode: 'development',
};


output.publicPath 中设置 JavaScript 的地址。
css-loWebPlugin.中设置被 CSS 导入的资源的的地址。
stylePublicPath 中设置 CSS 文件的地址。