> Webpack 在执行构建时默认会从项目根目录下的 webpack.config.js 文件读取配置

# Entry：

入口，Webpack 执行构建的第一步将从 Entry 开始，可抽象成输入。

# Module：

模块，在 Webpack 里一切皆模块，一个模块对应着一个文件。Webpack 会从配置的 Entry 开始递归找出所有依赖的模块。

# Chunk：

代码块，一个 Chunk 由多个模块组合而成，用于代码合并与分割。

# Loader：

模块转换器，用于把模块原内容按照需求转换成新内容。

# Plugin：

扩展插件，在 Webpack 构建流程中的特定时机注入扩展逻辑来改变构建结果或做你想要的事情。

# Output：

输出结果，在 Webpack 经过一系列处理并得出最终想要的代码后输出结果。

`Webpack 启动后会从 Entry 里配置的 Module 开始递归解析 Entry 依赖的所有 Module。 每找到一个 Module， 就会根据配置的 Loader 去找出对应的转换规则，对 Module 进行转换后，再解析出当前 Module 依赖的 Module。 这些模块会以 Entry 为单位进行分组，一个 Entry 和其所有依赖的 Module 被分到一个组也就是一个 Chunk。最后 Webpack 会把所有 Chunk 转换成文件输出。 在整个流程中 Webpack 会在恰当的时机执行 Plugin 里定义的逻辑。`

# 配置总结

> 通常你可用如下经验去判断如何配置 Webpack：

- 想让源文件加入到构建流程中去被 Webpack 控制，配置 entry。
- 想自定义输出文件的位置和名称，配置 output。
- 想自定义寻找依赖模块时的策略，配置 resolve。
- 想自定义解析和转换文件的策略，配置 module，通常是配置 module.rules 里的 Loader。
- 其它的大部分需求可能要通过 Plugin 去实现，配置 plugin。

# 实战总结

- 对所面临的问题本身要了解。例如在用 Webpack 去构建 React 应用时你需要先掌握 React 的基础知识。
- 找出现实和目标之间的差异。例如在 React 应用的源码中用到了 JSX 语法和 ES6 语法，需要把源码转换成 ES5。
- 找出从现实到目标的可能路径。例如把新语法转换成 ES5 可以使用 Babel 去转换源码。
- 搜索社区中有没有现成的针对可能路径的 Webpack 集成方案。例如社区中已经有 babel-loader。
- 如果找不到现成的方案说明你的需求非常特别，这时候你就需要编写自己的 Loader 或者 Plugin 了。在 第 5 章中会介绍如何编写它们。
- 在解决问题的过程中有以下 2 点能力很
