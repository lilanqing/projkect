const { resolve } = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');

module.exports = {
  // 单入口
  entry: './src/js/index.js',
  output: {
    // [name]：取文件名
    filename: 'js/[name].[contenthash:10].js',

    path: resolve(__dirname, 'build')
  },
  plugins: [
    new HtmlWebpackPlugin({
      template: './src/index.html',
      minify: {
        collapseWhitespace: true,
        removeComments: true
      }
    })
  ],
  /*
    1. 可以将node_modules中代码单独打包一个chunk最终输出
    2. 自动分析多入口chunk中，有没有公共的文件。如果有会打包成单独一个chunk
  */
  optimization: {
    splitChunks: {
      chunks: 'all'
    }











  },
  mode: 'production'
};

// splitchunks: {
//   minsize: 30000, chunks: 'all', maxsize: 0，//生成的块的最大大小(以字节为单位，maxAsyncRequests:30，//按需加载时的最大并行请求数maxInitialRequests:50，//入口点的最大并行请求数minChunks:1，// 分割前必须共享模块的最小块数cacheGroups:fcommons :test:/[\V]src[\V/]components[\V/]/name :'common-components',chunks: 'all',
// }，styles: {
//   'styles'yname: test: /\.css$/, chunks: 'all'enforce: true, L.
//     vendors: {
//       name: 'vendors'，// 提取出来的公共模块将会以这个命名test:/[\V/]node_modules[\\/]/，// 匹配node_modules中的模块priority:-10，//优先级，数字越大表示优先级越高，但因为是负数，所以这里chunks:'al1"，//指定哪些类型的chunks参与拆分reuseExistingchunk:true，// 如果这个模块已经被打包过，则忽略，不再打包
// default: fminChunks: 2, priority: -20, reuseExistingChunk: true，// 如果当前块包含已从主 bundle 中分离出的模块
//   }
// }