const loopMenuItem = (menus: MenuDataItem[] = []): MenuDataItem[] => {
  return menus.map(({ nodeData, children, ...item }) => {
    const { menuUrl, menuName, icon } = nodeData;
    return {
      ...item,
      key: menuUrl,
      path: menuUrl,
      name: menuName,
      // icon: icon && <AntdIcons name={nodeData.icon as keyof typeof antIcons} />,
      children: children && loopMenuItem(children),
    };
  });
};