http 缓存控制

1. http 缓存能够帮助服务器提高并发性能，很多资源不需要重复请求直接从浏览器中拿缓存
2. http 缓存分类 ：强缓存 协商缓存
3. 强缓存通过 expires 和 cache-control 控制,,, 协商缓存 通过 last-Modify（服务器返回） if-modify-since（客户端请求） 和 E-tag（服务端返回） if-none-match（客户端请求）控制

补充： 1. 为什么有 expires(http1.0) 有需要 cache-control(http1.1)
因为 expires 有个服务器和浏览器时间不同步的问题(绝对实践)
expires 是绝对时间,,, cache-control 是**相对时间**

    2. last-modify((http1.0))和Etag (http1.1)

     last-Modify（服务器返回）/if-modify-since（客户端请求）它是有个精度问题 到秒 ((http1.0))
     E-tag（服务端返回）/if-none-match（客户端请求）没有精度问题 只要文件改变  e-tag值就改变 (http1.1)

http2 新特性：

- 二进制分帧
- 头部压缩
- 多路复用
- 服务器推送
- 请求优先级
- 流量控制

资源
网络
代码
用户体验

闭包是由函数及其声明所在的词法环境结合而成。
闭包是指有权访问另一个函数作用域中的变量的函数。
闭包的作用域链：函数的词法作用域 + 函数的运行时作用域
闭包的作用：

1.  私有变量
2.  封装对象
3.  实现类
4.  实现模块
5.  实现私有方法
6.  实现回调函数
7.  实现迭代器
8.  实现单例
9.  实现观察者模式
10. 实现装饰器
11. 实现缓存
12. 实现异步
13. 实现异步回调
14. 实现异步加载

CSS 计算过程

- 确定声明
- 层叠冲突
  - 重要性
  - 特殊性(优先级)
  - 源次序
- 继承 (inherit)
- 默认值 (initial)

FP
FCP
LCP
FID
CLS
TTI
TBT
