# fetch vs axios

1. fetch 是一个底层的 api 浏览器原生支持的 axios 是一个封装好的框架
2. axios 1）支持浏览器和 nodejs 发请求 前后端发请求，
   2）支持 promise 语法  
    3）支持自动解析 json  
    4）支持中断请求
   5） 支持拦截请求
   6） 支持请求进度监测
   7） 支持客户端防止 csrf

   一句话总结： 封装比较好

3. fetch  
   优点： 1. 浏览器级别原生支持的 api 2. 原生支持 promise api 3. 语法简洁 符合 es 标准规范 4. 是由 whatwg 组织提出的 现在已经是 w3c 规范

   缺点： 1. 不支持文件上传进度监测 2. 使用不完美 需要封装 3. 不支持请求中止 4. 默认不带 cookie
   一句话总结： 缺点是需要封装 优点 底层原生支持

   <!-- CDN 地址：https://cdnjs.cloudflare.com/ajax/libs/fetch/3.0.0/fetch.min.js -->
