# 浏览器多页签通讯实现

1. 可以借助 浏览器 localstorage 方式实现 cookie + setInterval 实现 websocket 全双工实现 sharedworker 实现
2. 1） localstorage 如何实现 同源
   localstorage.setItem 方法传数据
   监听 window 上 storage 事件 就可以获得数据 2) cookie + setInterval  
    document.cookie 发数据
   setInterval 不停地去 cookie 上去数据 3) websocket 实现 跨源
   websocket 是全双工通讯方式 多页签可以将服务器作为桥梁来实现通讯 跨源
   4）h5 新技术 共享 worker sharedworker 也可以实现 同源
   5） 如果我们能够获得对应标签页的引⽤可以使用 window.postMessage() 跨源

# 浏览器多页签通讯实现

// 有同源限制
localstorage 实现
servicevorker 实现
indexedDB 实现
MessageChannel 实现
BoradcastChannel 实现
sharedworker 实现
cookie + setInterval

// 无同源限制
window.postMessage()
websocket 全双工实现
